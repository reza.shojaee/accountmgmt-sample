package com.mobilab.masterdata.accountmgmt.data.dto;

import com.mobilab.masterdata.accountmgmt.data.entity.Account;

import static com.mobilab.masterdata.accountmgmt.util.json.JsonUtil.deserialize;
import static com.mobilab.masterdata.accountmgmt.util.reflect.ReflectionUtil.setCorrespondingProperties;

/**
 * @author Reza Shojaee, 11/15/18 11:00 PM
 */
public class AccountDto extends Account {
    public AccountDto(Account account) {
        setCorrespondingProperties(account, this);
    }

    public AccountDto() {}

    // Used by jackson for json automatic object parsing as parameters of api methods
    public static AccountDto fromString(String serializedAccount) {
        return deserialize(serializedAccount, AccountDto.class);
    }
}
