package com.mobilab.masterdata.accountmgmt.data.dto;

import com.mobilab.masterdata.accountmgmt.data.entity.FundsTransferTx;

import static com.mobilab.masterdata.accountmgmt.util.json.JsonUtil.deserialize;
import static com.mobilab.masterdata.accountmgmt.util.reflect.ReflectionUtil.setCorrespondingProperties;

/**
 * @author Reza Shojaee, 11/15/18 11:00 PM
 */
public class FundsTransferTxDto extends FundsTransferTx {
    public FundsTransferTxDto(FundsTransferTx fundsTransferTx) {
        setCorrespondingProperties(fundsTransferTx, this);
    }

    public FundsTransferTxDto() {}

    // Used by jackson for json automatic object parsing as parameters of api methods
    public static FundsTransferTxDto fromString(String serializedFundsTransferTx) {
        return deserialize(serializedFundsTransferTx, FundsTransferTxDto.class);
    }
}
