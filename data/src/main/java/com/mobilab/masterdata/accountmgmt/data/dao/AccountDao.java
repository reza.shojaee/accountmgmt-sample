package com.mobilab.masterdata.accountmgmt.data.dao;

import com.mobilab.masterdata.accountmgmt.data.entity.Account;
import com.mobilab.masterdata.accountmgmt.data.exception.InsufficientBalanceException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.math.BigDecimal;

/**
 * @author Reza Shojaee, 11/15/18 11:54 PM
 */
public class AccountDao extends BaseDao<Account> {
    @PersistenceContext(name = "account-management", unitName = "account-management")
    private EntityManager entityManager;

    public AccountDao() {
        super(Account.class);
    }

    public Account retrieveByAccountNo(String accountNo) {
        TypedQuery<Account> typedQuery = entityManager.createQuery(
                "select a from Account a where a.accountNo = :accountNo", Account.class);
        typedQuery.setParameter("accountNo", accountNo);
        return typedQuery.getSingleResult();
    }

    public void withdraw(Account account, BigDecimal amount) {
        lockPessimistic(account.getId());
        BigDecimal accountCurrentBalance = account.getCurrentBalance();
        BigDecimal balanceAfterWithdrawal = accountCurrentBalance.subtract(amount);
        if (balanceAfterWithdrawal.compareTo(BigDecimal.ZERO) < 0)
            throw new InsufficientBalanceException();
        account.setCurrentBalance(balanceAfterWithdrawal);
        update(account);
    }

    public void deposit(Account account, BigDecimal amount) {
        lockPessimistic(account.getId());
        account.setCurrentBalance(account.getCurrentBalance().add(amount));
        update(account);
    }

    @Override
    protected EntityManager getEntityManager() {
        return entityManager;
    }
}
