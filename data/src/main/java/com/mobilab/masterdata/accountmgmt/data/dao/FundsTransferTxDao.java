package com.mobilab.masterdata.accountmgmt.data.dao;

import com.mobilab.masterdata.accountmgmt.data.entity.Account;
import com.mobilab.masterdata.accountmgmt.data.entity.FundsTransferTx;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @author Reza Shojaee, 11/15/18 11:56 PM
 */
public class FundsTransferTxDao extends BaseDao<FundsTransferTx> {
    @PersistenceContext(name = "account-management", unitName = "account-management")
    EntityManager entityManager;

    public FundsTransferTxDao() {
        super(FundsTransferTx.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return entityManager;
    }
}
