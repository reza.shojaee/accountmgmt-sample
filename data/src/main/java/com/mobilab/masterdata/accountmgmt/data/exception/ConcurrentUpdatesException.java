package com.mobilab.masterdata.accountmgmt.data.exception;

/**
 * @author Reza Shojaee, 11/15/18 11:54 PM
 */
@SuppressWarnings({ "unused", "WeakerAccess" })
public class ConcurrentUpdatesException extends BusinessLogicException {
    public ConcurrentUpdatesException() {}

    public ConcurrentUpdatesException(String message) {
        super(message);
    }

    public ConcurrentUpdatesException(Exception ex) {
        super(ex);
    }

    public ConcurrentUpdatesException(String message, Exception ex) {
        super(message, ex);
    }
}
