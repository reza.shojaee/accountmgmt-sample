package com.mobilab.masterdata.accountmgmt.data.dao;

import com.mobilab.masterdata.accountmgmt.data.entity.BaseEntity;
import com.mobilab.masterdata.accountmgmt.data.exception.ConcurrentUpdatesException;
import com.mobilab.masterdata.accountmgmt.data.exception.NonexistentRecordException;
import com.mobilab.masterdata.accountmgmt.data.exception.NotPermittedException;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;
import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import static com.mobilab.masterdata.accountmgmt.data.entity.BaseEntity.ID_ATTRIBUTE;
import static com.mobilab.masterdata.accountmgmt.data.entity.BaseEntity.LOCK_VERSION_ATTRIBUTE;
import static com.mobilab.masterdata.accountmgmt.util.reflect.ReflectionUtil.findPersistentPropertiesValues;

/**
 * @author Reza Shojaee, 8/3/18 3:50 PM
 */
public abstract class BaseDao<T extends BaseEntity> implements Dao<T> {
    private static final long SEED_RECORDS_START_ID = 1;
    private static final long SEED_RECORDS_END_ID = 999;
    private static Set<String> updateExcludeFields =
            new HashSet<>(Arrays.asList(ID_ATTRIBUTE, LOCK_VERSION_ATTRIBUTE));
    private Class<T> entityClass;

    @SuppressWarnings("WeakerAccess")
    public BaseDao(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    @Override
    public void store(T entity) {
        protectSeedRecords(entity);
        entity.setLockVersion(1);
        getEntityManager().persist(entity);
    }

    @Override
    public List<T> retrieveAll() {
        CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(entityClass);
        Root<T> rootEntity = criteriaQuery.from(entityClass);
        CriteriaQuery<T> query = criteriaQuery.select(rootEntity);
        return getEntityManager().createQuery(query).getResultList();
    }

    @Override
    public T retrieve(long id) {
        return getEntityManager().find(entityClass, id);
    }

    @Override
    public T getReference(long id) {
        return getEntityManager().getReference(entityClass, id);
    }

    @Override
    public T update(T entity) {
        protectSeedRecords(entity);
        getEntityManager().merge(entity);
        // NB! unfortunately merge() does not reflect last state of version correctly
        entity.setLockVersion(entity.getLockVersion() + 1);
        return entity;
    }

    @Override
    public T patch(T entity) {
        // NB Optimistic lock must be handled manually by ourselves
        Entry<CriteriaBuilder, Entry<CriteriaUpdate<T>, Root<T>>> criteria = makeCriteria(entity);
        CriteriaBuilder criteriaBuilder = criteria.getKey();
        CriteriaUpdate<T> criteriaUpdate = criteria.getValue().getKey();
        Root<T> rootEntity = criteria.getValue().getValue();
        criteriaUpdate.where(criteriaBuilder.equal(rootEntity.get(ID_ATTRIBUTE), entity.getId()));
        criteriaUpdate.where(criteriaBuilder.equal(
                rootEntity.get(LOCK_VERSION_ATTRIBUTE), entity.getLockVersion()));
        // NB Don't forget to update optimistic lock field for subsequent updates
        criteriaUpdate.set(LOCK_VERSION_ATTRIBUTE, entity.getLockVersion() + 1);
        int count = getEntityManager().createQuery(criteriaUpdate).executeUpdate();
        if (count == 0) {
            entity = getEntityManager().find(entityClass, entity.getId());
            if (entity == null)
                throw new NonexistentRecordException();
            throw new ConcurrentUpdatesException();
        }
        // NB! unfortunately merge() does not reflect last state of version correctly
        entity.setLockVersion(entity.getLockVersion() + 1);
        // NB! financial transactions performance warning: do not do below, move the responsibility to caller
        return entity; // getEntityManager().find(entityClass, entity.getId());
    }

    @Override
    public void delete(T entity) {
        protectSeedRecords(entity);
        getEntityManager().remove(getEntityManager().getReference(entityClass, entity.getId()));
    }

    @Override
    public void delete(long id) {
        protectSeedRecords(id);
        getEntityManager().remove(getEntityManager().getReference(entityClass, id));
    }

    @Override
    public long count() {
        CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        criteriaQuery.select(criteriaBuilder.count(criteriaQuery.from(entityClass)));
        return getEntityManager().createQuery(criteriaQuery).getSingleResult();
    }

    @Override
    public T lockEntity(long id, boolean optimisticLock) {
        T attachedEntity = getEntityManager().getReference(entityClass, id);
        getEntityManager().lock(attachedEntity,
                optimisticLock ? LockModeType.OPTIMISTIC : LockModeType.PESSIMISTIC_WRITE);
        return attachedEntity;
    }

    private Entry<CriteriaBuilder, Entry<CriteriaUpdate<T>, Root<T>>> makeCriteria(T entity) {
        CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
        CriteriaUpdate<T> criteriaUpdate = criteriaBuilder.createCriteriaUpdate(entityClass);
        // NB! Due to weird design by hibernate guys, criteriaUpdate.from() MUST come before criteriaUpdate.set()
        Root<T> rootEntity = criteriaUpdate.from(entityClass);
        findPersistentPropertiesValues(entity).entrySet().stream()
                .filter(entry -> entry.getValue() != null
                        && !updateExcludeFields.contains(entry.getKey()))
                .forEach(entry -> criteriaUpdate.set(entry.getKey(), entry.getValue()));
        return new SimpleImmutableEntry<>(criteriaBuilder,
                new SimpleImmutableEntry<>(criteriaUpdate, rootEntity));
    }

    private void protectSeedRecords(Long id) {
        if (id != null && id >= SEED_RECORDS_START_ID && id <= SEED_RECORDS_END_ID)
            throw new NotPermittedException(
                    "Seed records are protected and cannot get created/updated/deleted, id: " + id);
    }

    private void protectSeedRecords(T entity) {
        protectSeedRecords(entity.getId());
    }

    protected abstract EntityManager getEntityManager();
}
