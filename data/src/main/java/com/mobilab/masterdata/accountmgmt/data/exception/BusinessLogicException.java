package com.mobilab.masterdata.accountmgmt.data.exception;

import com.mobilab.masterdata.accountmgmt.util.exception.ExceptionContextMixIn;

import javax.ejb.EJBException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Reza Shojaee, 7/12/18 12:30 AM
 */
@SuppressWarnings({ "unused", "WeakerAccess" })
public class BusinessLogicException extends EJBException
        implements ExceptionContextMixIn<BusinessLogicException> {
    public static final String UPSTREAM_CONNECT_FAILED = "UPSTREAM_CONNECT_FAILED";
    public static final String UPSTREAM_RESPONSE_TIMEOUT = "UPSTREAM_RESPONSE_TIMEOUT";
    public static final String UPSTREAM_ERROR_RESPONSE = "UPSTREAM_ERROR_RESPONSE";
    public static final String UNIQUE_ENTITY = "UNIQUE_ENTITY";
    public static final String NO_UNIQUE_ENTITY = "NO_UNIQUE_ENTITY";
    public static final String MULTIPLE_ENTITIES_FOR_UNIQUE = "MULTIPLE_ENTITIES_FOR_UNIQUE";

    private String errorCode;
    private String userMessage;
    private List<String> errorFields = new ArrayList<>();
    private List<String> errorConditions = new ArrayList<>();

    public BusinessLogicException() {}

    public BusinessLogicException(String message) {
        super(message);
    }

    public BusinessLogicException(Exception e) {
        super(e);
    }

    public BusinessLogicException(String message, Exception e) {
        super(message, e);
    }

    public String getErrorCode() {
        return errorCode;
    }

    @Override
    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getUserMessage() {
        return userMessage;
    }

    @Override
    public void setUserMessage(String userMessage) {
        this.userMessage = userMessage;
    }

    @Override
    public List<String> getErrorFields() {
        return errorFields;
    }

    public void setErrorFields(List<String> errorFields) {
        this.errorFields = errorFields;
    }

    @Override
    public List<String> getErrorConditions() {
        return errorConditions;
    }

    public void setErrorConditions(List<String> errorConditions) {
        this.errorConditions = errorConditions;
    }
}
