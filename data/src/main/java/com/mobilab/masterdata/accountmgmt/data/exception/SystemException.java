package com.mobilab.masterdata.accountmgmt.data.exception;

import com.mobilab.masterdata.accountmgmt.util.exception.GenericException;

/**
 * @author Reza Shojaee, 11/15/18 11:50 PM
 */
public class SystemException extends GenericException {
    public SystemException() {}

    public SystemException(String message) {
        super(message);
    }

    public SystemException(String message, Throwable cause) {
        super(message, cause);
    }

    public SystemException(Throwable cause) {
        super(cause);
    }

    public SystemException(
            String message, Throwable cause,
            boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
