package com.mobilab.masterdata.accountmgmt.data.exception;

/**
 * @author Reza Shojaee, 11/15/18 11:52 PM
 */
@SuppressWarnings({ "unused", "WeakerAccess" })
public class NotPermittedException extends BusinessLogicException {
    public NotPermittedException() {}

    public NotPermittedException(String message) {
        super(message);
    }

    public NotPermittedException(Exception ex) {
        super(ex);
    }

    public NotPermittedException(String message, Exception ex) {
        super(message, ex);
    }
}
