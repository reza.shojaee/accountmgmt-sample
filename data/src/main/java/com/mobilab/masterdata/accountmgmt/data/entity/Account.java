package com.mobilab.masterdata.accountmgmt.data.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Calendar;

/**
 * @author Reza Shojaee, 11/15/18 10:39 PM
 */
@Entity
@Table(name = "ACCOUNT")
@SequenceGenerator(name = "sequence-generator", sequenceName = "ACCOUNT_SEQ",
        initialValue = 10000, allocationSize = 1000)
public class Account extends BaseEntity {
    private String accountNo;
    private String currencyCode;
    private BigDecimal currentBalance;
    private String ownerName;
    private String remarks;
    private Calendar timestamp;

    @Column(name = "ACCOUNT_NO")
    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    @Column(name = "CURRENCY_CODE")
    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    @Column(name = "CURRENT_BALANCE")
    public BigDecimal getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(BigDecimal currentBalance) {
        this.currentBalance = currentBalance;
    }

    @Column(name = "OWNER_NAME")
    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    @Column(name = "REMARKS")
    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Column(name = "TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    public Calendar getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Calendar timestamp) {
        this.timestamp = timestamp;
    }
}
