package com.mobilab.masterdata.accountmgmt.data.exception;

/**
 * @author Reza Shojaee, 11/15/18 11:51 PM
 */
@SuppressWarnings({ "unused", "WeakerAccess" })
public class NonexistentRecordException extends BusinessLogicException {
    public NonexistentRecordException() {}

    public NonexistentRecordException(String message) {
        super(message);
    }

    public NonexistentRecordException(Exception ex) {
        super(ex);
    }

    public NonexistentRecordException(String message, Exception ex) {
        super(message, ex);
    }
}
