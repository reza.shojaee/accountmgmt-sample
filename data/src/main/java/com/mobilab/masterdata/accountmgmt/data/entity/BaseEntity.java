package com.mobilab.masterdata.accountmgmt.data.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

import static com.mobilab.masterdata.accountmgmt.util.json.JsonUtil.serialize;

/**
 * @author Reza Shojaee, 11/15/18 6:14 PM
 */
@MappedSuperclass
public abstract class BaseEntity implements Serializable, Cloneable {
    public static final String ID_ATTRIBUTE = "id";
    public static final String LOCK_VERSION_ATTRIBUTE = "lockVersion";

    private Long id;
    private Integer lockVersion;

    public BaseEntity(Long id) {
        this.id = id;
    }

    public BaseEntity() {}

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "sequence-generator")
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "LOCK_VERSION")
    @Version
    public Integer getLockVersion() {
        return lockVersion;
    }

    public void setLockVersion(Integer entityVersion) {
        this.lockVersion = entityVersion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        BaseEntity entity = (BaseEntity) o;
        return id.equals(entity.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return serialize(this);
    }
}
