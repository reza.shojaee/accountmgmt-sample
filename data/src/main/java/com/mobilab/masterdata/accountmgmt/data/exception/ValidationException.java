package com.mobilab.masterdata.accountmgmt.data.exception;

/**
 * @author Reza Shojaee, 11/15/18 11:55 PM
 */
@SuppressWarnings({ "unused", "WeakerAccess" })
public class ValidationException extends BusinessLogicException {
    public ValidationException() {}

    public ValidationException(String message) {
        super(message);
    }

    public ValidationException(Exception ex) {
        super(ex);
    }

    public ValidationException(String message, Exception ex) {
        super(message, ex);
    }
}
