package com.mobilab.masterdata.accountmgmt.data.exception;

/**
 * @author Reza Shojaee, 11/16/18 10:30 PM
 */
@SuppressWarnings({ "unused", "WeakerAccess" })
public class InsufficientBalanceException extends BusinessLogicException {
    public InsufficientBalanceException() {}

    public InsufficientBalanceException(String message) {
        super(message);
    }

    public InsufficientBalanceException(Exception e) {
        super(e);
    }

    public InsufficientBalanceException(String message, Exception e) {
        super(message, e);
    }
}
