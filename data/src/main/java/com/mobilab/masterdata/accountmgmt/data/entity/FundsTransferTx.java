package com.mobilab.masterdata.accountmgmt.data.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Calendar;

/**
 * @author Reza Shojaee, 11/15/18 10:50 PM
 */
@Entity
@Table(name = "FUNDS_TRANSFER_TX")
@SequenceGenerator(name = "sequence-generator", sequenceName = "FUNDS_TRANSFER_TX_SEQ",
        initialValue = 10000, allocationSize = 1000)
public class FundsTransferTx extends BaseEntity {
    private Account sourceAccount;
    private Account destinationAccount;
    private BigDecimal amount;
    private BigDecimal exchangeRate;
    private String traceCode;
    private String responseCode;
    private String detailCode;
    private Calendar requestTimestamp;
    private Calendar responseTimestamp;
    private Calendar storeTimestamp;

    @ManyToOne
    @JoinColumn(name = "SOURCE_ACCOUNT_ID")
    public Account getSourceAccount() {
        return sourceAccount;
    }

    public void setSourceAccount(Account sourceAccount) {
        this.sourceAccount = sourceAccount;
    }

    @ManyToOne
    @JoinColumn(name = "DESTINATION_ACCOUNT_ID")
    public Account getDestinationAccount() {
        return destinationAccount;
    }

    public void setDestinationAccount(Account destinationAccount) {
        this.destinationAccount = destinationAccount;
    }

    @Column(name = "AMOUNT")
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Column(name = "EXCHANGE_RATE")
    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    @Column(name = "TRACE_CODE")
    public String getTraceCode() {
        return traceCode;
    }

    public void setTraceCode(String traceCode) {
        this.traceCode = traceCode;
    }

    @Column(name = "RESPONSE_CODE")
    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    @Column(name = "DETAIL_CODE")
    public String getDetailCode() {
        return detailCode;
    }

    public void setDetailCode(String detailCode) {
        this.detailCode = detailCode;
    }

    @Column(name = "REQUEST_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    public Calendar getRequestTimestamp() {
        return requestTimestamp;
    }

    public void setRequestTimestamp(Calendar requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }

    @Column(name = "RESPONSE_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    public Calendar getResponseTimestamp() {
        return responseTimestamp;
    }

    public void setResponseTimestamp(Calendar responseTimestamp) {
        this.responseTimestamp = responseTimestamp;
    }

    @Column(name = "STORE_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    public Calendar getStoreTimestamp() {
        return storeTimestamp;
    }

    public void setStoreTimestamp(Calendar storeTimestamp) {
        this.storeTimestamp = storeTimestamp;
    }
}
