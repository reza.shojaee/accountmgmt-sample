package com.mobilab.masterdata.accountmgmt.data.dao;

import java.util.List;

/**
 * @author Reza Shojaee, 11/15/18 11:30 PM
 */
public interface Dao<T> {
    void store(T entity);

    List<T> retrieveAll();

    T retrieve(long id);

    T getReference(long id);

    T update(T entity);

    T patch(T entity);

    void delete(T entity);

    void delete(long id);

    long count();

    T lockEntity(long id, boolean optimisticLock);

    default T lockOptimistic(long id) {
        return lockEntity(id, true);
    }

    default T lockPessimistic(long id) {
        return lockEntity(id, false);
    }
}
