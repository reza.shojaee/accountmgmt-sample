
create table if not exists funds_transfer_tx (
  id number(18) not null,
  source_account_id number(18) not null,
  destination_account_id number(18) not null,
  amount number(12,2) not null,
  exchange_rate number(14,4) not null,
  trace_code nvarchar2(30) not null,
  response_code char(3),
  detail_code char(3),
  request_timestamp timestamp(3) default sysdate not null,
  response_timestamp timestamp(3) default sysdate,
  store_timestamp timestamp(3) default sysdate not null,
  lock_version number(9) default 1 not null,
  constraint funds_transfer_pk primary key (id),
  constraint funds_transfer_fk_source_account_id
    foreign key (source_account_id) references account (id),
  constraint funds_transfer_fk_destination_account_id
    foreign key (destination_account_id) references account (id),
  constraint funds_transfer_uk_trace_code unique (trace_code)
);
create sequence if not exists funds_transfer_tx_seq start with 10000 increment by 1000 cache 100;
