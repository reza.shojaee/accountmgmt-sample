
create table if not exists account (
  id number(18) not null,
  account_no varchar2(26) not null,
  currency_code char(3) not null,
  current_balance number(12,2) not null,
  owner_name nvarchar2(200), -- actually it should be a foreign key reference, anyways for now!
  remarks nvarchar2(2000),
  timestamp timestamp(3) default sysdate not null,
  lock_version number(9) default 1 not null,
  constraint account_pk primary key (id),
  constraint account_uk_account_no unique (account_no),
  constraint account_ch_currency_code check (currency_code regexp '[A-Z]{3}'), -- regexp_like()
  constraint account_ch_current_balance check (current_balance > 0)
);
create sequence if not exists account_seq start with 10000 increment by 1000 cache 100;

create index if not exists account_ux_owner_name on account (owner_name);
