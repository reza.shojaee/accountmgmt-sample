package com.mobilab.masterdata.accountmgmt.data.dao;

import com.mobilab.masterdata.accountmgmt.data.entity.FundsTransferTx;

/**
 * @author Reza Shojaee, 11/16/18 12:01 AM
 */
public class FundsTransferTxDaoTest extends BaseDaoTest<FundsTransferTx> {
    public FundsTransferTxDaoTest() {
        super(FundsTransferTx.class);
    }

    @Override
    BaseDao<FundsTransferTx> makeDaoInstance() {
        return new FundsTransferTxDao();
    }

    @Override
    FundsTransferTx makeEntityInstance() {
        return new FundsTransferTx();
    }
}
