package com.mobilab.masterdata.accountmgmt.data.dao;

import com.mobilab.masterdata.accountmgmt.data.entity.BaseEntity;
import com.mobilab.masterdata.accountmgmt.data.exception.ConcurrentUpdatesException;
import com.mobilab.masterdata.accountmgmt.data.exception.NonexistentRecordException;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

import static com.mobilab.masterdata.accountmgmt.util.reflect.ReflectionUtil.setFieldValue;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

/**
 * @author Reza Shojaee, 11/16/18 3:41 AM
 */
public abstract class BaseDaoTest<T extends BaseEntity> {
    private static final long TEST_ID = 1000;

    BaseDao<T> dao;
    private Class<T> entityClass;

    BaseDaoTest(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    @Before
    public void setUp() {
        dao = makeDaoInstance();
        setFieldValue(dao, "entityManager", mock(EntityManager.class));
    }

    @Test
    public void store() {
        T entity = makeEntityInstance();
        doNothing().when(dao.getEntityManager()).persist(any());
        dao.store(entity);
        verify(dao.getEntityManager()).persist(entity);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void retrieveAll() {
        List<T> entities = new ArrayList<>();
        T entity = mock(entityClass);
        when(entity.getId()).thenReturn(TEST_ID);
        entities.add(entity);
        when(dao.getEntityManager().getCriteriaBuilder())
                .thenReturn(mock(CriteriaBuilder.class));
        CriteriaQuery<T> mockedCriteriaQuery = mock(CriteriaQuery.class);
        when(dao.getEntityManager().getCriteriaBuilder().createQuery(entityClass))
                .thenReturn(mockedCriteriaQuery);
        Root<T> mockedRoot = mock(Root.class);
        when(mockedCriteriaQuery.from(entityClass)).thenReturn(mockedRoot);
        when(mockedCriteriaQuery.select(mockedRoot)).thenReturn(mockedCriteriaQuery);
        TypedQuery<T> mockedTypedQuery = mock(TypedQuery.class);
        when(dao.getEntityManager().createQuery(mockedCriteriaQuery))
                .thenReturn(mockedTypedQuery);
        //when(mockedTypedQuery.setParameter(anyString(), any())).thenReturn(mockedTypedQuery);
        when(mockedTypedQuery.getResultList()).thenReturn(entities);
        entities = dao.retrieveAll();
        assertThat(entities, is(not(nullValue())));
    }

    @Test
    public void retrieveById() {
        T expectedEntity = makeEntityInstance();
        when(dao.getEntityManager().find(any(), any()))
                .thenReturn(expectedEntity);
        T retrievedEntity = dao.retrieve(TEST_ID);
        assertThat(retrievedEntity, is(equalTo(expectedEntity)));
    }

    @Test
    public void getEntityReference() {
        T expectedEntity = makeEntityInstance();
        when(dao.getEntityManager().getReference(any(), any()))
                .thenReturn(expectedEntity);
        T entityReference = dao.getReference(TEST_ID);
        assertThat(entityReference, is(equalTo(expectedEntity)));
    }

    @Test
    public void update() {
        T entityToUpdate = makeEntityInstance();
        entityToUpdate.setLockVersion(1);
        when(dao.getEntityManager().merge(entityToUpdate))
                .thenReturn(entityToUpdate);
        T updatedEntity = dao.update(entityToUpdate);
        verify(dao.getEntityManager()).merge(entityToUpdate);
        assertThat(updatedEntity, is(equalTo(entityToUpdate)));
        assertThat(updatedEntity.getLockVersion(), is(equalTo(2)));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void patch() {
        T entityToPatch = makeEntityInstance();
        entityToPatch.setLockVersion(1);
        CriteriaBuilder criteriaBuilder = mock(CriteriaBuilder.class);
        when(dao.getEntityManager().getCriteriaBuilder()).thenReturn(criteriaBuilder);
        CriteriaUpdate<T> mockedCriteriaUpdate = mock(CriteriaUpdate.class);
        when(dao.getEntityManager().getCriteriaBuilder().createCriteriaUpdate(entityClass))
                .thenReturn(mockedCriteriaUpdate);
        Root<T> mockedRoot = mock(Root.class);
        when(mockedCriteriaUpdate.from(entityClass)).thenReturn(mockedRoot);
        when(mockedCriteriaUpdate.set(anyString(), any())).thenReturn(mockedCriteriaUpdate);
        Path path = mock(Path.class);
        when(mockedRoot.get(anyString())).thenReturn(path);
        Predicate predicate = mock(Predicate.class);
        when(criteriaBuilder.equal(any(Path.class), any())).thenReturn(predicate);
        when(mockedCriteriaUpdate.where(predicate)).thenReturn(mockedCriteriaUpdate);
        Query mockedQuery = mock(Query.class);
        when(dao.getEntityManager().createQuery(mockedCriteriaUpdate))
                .thenReturn(mockedQuery);
        when(mockedQuery.executeUpdate()).thenReturn(1);
        T patchedEntity = dao.patch(entityToPatch);
        assertThat(patchedEntity, is(equalTo(entityToPatch)));
        assertThat(patchedEntity.getLockVersion(), is(equalTo(2)));

        // Also test the situation where entity does not exist to be updated
        when(mockedQuery.executeUpdate()).thenReturn(0);
        when(dao.getEntityManager().find(any(), any())).thenReturn(null);
        try {
            dao.patch(entityToPatch);
        } catch (Exception e) {
            assertThat(e instanceof NonexistentRecordException, is(true));
        }

        // Also test the situation where entity already modified by someone else
        when(mockedQuery.executeUpdate()).thenReturn(0);
        when(dao.getEntityManager().find(any(), any())).thenReturn(entityToPatch);
        try {
            dao.patch(entityToPatch);
        } catch (Exception e) {
            assertThat(e instanceof ConcurrentUpdatesException, is(true));
        }
    }

    @Test
    public void delete() {
        T entityToDelete = makeEntityInstance();
        doNothing().when(dao.getEntityManager()).remove(entityToDelete);
        dao.delete(entityToDelete);
        verify(dao.getEntityManager()).remove(any());
    }

    @Test
    public void deleteById() {
        T entity = makeEntityInstance();
        when(dao.getEntityManager().getReference(any(), any()))
                .thenReturn(entity);
        doNothing().when(dao.getEntityManager()).remove(entity);
        dao.delete(TEST_ID);
        verify(dao.getEntityManager()).remove(entity);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void count() {
        when(dao.getEntityManager().getCriteriaBuilder())
                .thenReturn(mock(CriteriaBuilder.class));
        CriteriaQuery<Long> mockedCriteriaQuery = mock(CriteriaQuery.class);
        when(dao.getEntityManager().getCriteriaBuilder().createQuery(Long.class))
                .thenReturn(mockedCriteriaQuery);
        Root<Long> mockedRoot = mock(Root.class);
        when(mockedCriteriaQuery.from(Long.class)).thenReturn(mockedRoot);
        when(mockedCriteriaQuery.select(mockedRoot)).thenReturn(mockedCriteriaQuery);
        TypedQuery<Long> mockedTypedQuery = mock(TypedQuery.class);
        when(dao.getEntityManager().createQuery(mockedCriteriaQuery))
                .thenReturn(mockedTypedQuery);
        when(mockedTypedQuery.getSingleResult()).thenReturn(1L);
        assertThat(dao.count(), is(equalTo(1L)));
    }

    @Test
    public void lockEntity() {
        T lockedEntity = makeEntityInstance();
        when(dao.getEntityManager().getReference(any(), any()))
                .thenReturn(lockedEntity);
        doNothing().when(dao.getEntityManager())
                .lock(any(entityClass), any(LockModeType.class));
        T entityReference = dao.lockEntity(TEST_ID, true);
        assertThat(entityReference, is(equalTo(lockedEntity)));
    }

    abstract BaseDao<T> makeDaoInstance();

    abstract T makeEntityInstance();
}
