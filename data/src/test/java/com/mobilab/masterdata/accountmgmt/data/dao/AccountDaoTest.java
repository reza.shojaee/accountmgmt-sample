package com.mobilab.masterdata.accountmgmt.data.dao;

import com.mobilab.masterdata.accountmgmt.data.entity.Account;
import com.mobilab.masterdata.accountmgmt.data.exception.InsufficientBalanceException;
import org.junit.Test;

import javax.persistence.LockModeType;
import javax.persistence.TypedQuery;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

/**
 * @author Reza Shojaee, 11/16/18 12:00 AM
 */
public class AccountDaoTest extends BaseDaoTest<Account> {
    public AccountDaoTest() {
        super(Account.class);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void retrieveByAccountNo() {
        TypedQuery<Object> mockedTypedQuery = mock(TypedQuery.class);
        when(dao.getEntityManager().createQuery(anyString(), any()))
                .thenReturn(mockedTypedQuery);
        when(mockedTypedQuery.setParameter(anyString(), any())).thenReturn(mockedTypedQuery);
        Account expected = makeEntityInstance();
        when(mockedTypedQuery.getSingleResult()).thenReturn(expected);
        Account actual = ((AccountDao) dao).retrieveByAccountNo("1");
        assertThat(actual, is(equalTo(expected)));
    }

    @Test
    public void withdrawNormally() {
        Account account = mockLocking();
        when(account.getCurrentBalance()).thenReturn(new BigDecimal("100.5"));
        ((AccountDao) dao).withdraw(account, new BigDecimal("10.5"));
        verify(account, times(1)).setCurrentBalance(new BigDecimal("90.0"));
    }

    @Test(expected = InsufficientBalanceException.class)
    public void withdrawInsufficientBalance() {
        Account account = mockLocking();
        when(account.getCurrentBalance()).thenReturn(new BigDecimal("9"));
        ((AccountDao) dao).withdraw(account, new BigDecimal("10"));
    }

    @Test
    public void deposit() {
        Account account = mockLocking();
        when(account.getCurrentBalance()).thenReturn(new BigDecimal("100.51"));
        ((AccountDao) dao).deposit(account, new BigDecimal("10.49"));
        verify(account, times(1)).setCurrentBalance(new BigDecimal("111.00"));
    }

    private Account mockLocking() {
        Account account = mock(Account.class);
        when(dao.getEntityManager().getReference(any(), any())).thenReturn(account);
        doNothing().when(dao.getEntityManager())
                .lock(any(Account.class), any(LockModeType.class));
        return account;
    }

    @Override
    BaseDao<Account> makeDaoInstance() {
        return new AccountDao();
    }

    @Override
    Account makeEntityInstance() {
        return new Account();
    }
}
