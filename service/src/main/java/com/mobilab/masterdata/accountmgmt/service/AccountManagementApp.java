package com.mobilab.masterdata.accountmgmt.service;

import org.apache.commons.io.IOUtils;
import org.h2.jdbc.JdbcSQLException;
import org.h2.tools.Server;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.annotation.sql.DataSourceDefinition;
import javax.sql.DataSource;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.io.IOException;
import java.io.InputStream;
import java.net.BindException;
import java.sql.SQLException;
import java.util.logging.Logger;

import static com.mobilab.masterdata.accountmgmt.util.logging.LogUtil.logError;
import static com.mobilab.masterdata.accountmgmt.util.logging.LogUtil.logInfo;
import static java.lang.Thread.currentThread;

/**
 * @author Reza Shojaee, 11/16/18 4:01 PM
 */
@ApplicationPath("v1")
@DataSourceDefinition(
        name = "java:/jdbc/account-management-data-source",
        className = "org.h2.jdbcx.JdbcDataSource",
        url = "jdbc:h2:tcp://localhost/~/accountmgmt",
        user = "sa",
        password = "sa",
        initialPoolSize = 0,
        minPoolSize = 1,
        maxPoolSize = 20)
public class AccountManagementApp extends Application {
    private static final Logger LOGGER = Logger.getLogger(AccountManagementApp.class.getName());

    @Resource(mappedName = "java:/jdbc/account-management-data-source")
    private DataSource dataSource;
    private Server dbServer;

    private static int instances = 0;

    @PostConstruct
    public void initializeSystem() {
        if (instances > 0)
            return;
        synchronized (this) {
            if (instances > 0)
                return;
            instances++;
            /* Take the chance and try to do one-off initializations for the entire app before it
            starts servicing. The kind of works below are here to get automatically done without
            some admin intervention, though these are the kind of works normally under control
            of system admin. Also take care to try to do this type of setups just one time and not
            on every instance of this object or application (hence the locking above) */
            try {
                startDbServerUp();
            } catch (Throwable t) {
                logError(LOGGER, this, t);
            }
            try {
                createTablesIfNotExist();
            } catch (Throwable t) {
                logError(LOGGER, this, t);
            }
        }
    }

    @PreDestroy
    public void preDestroy() {
        if (dbServer != null)
            dbServer.stop();
    }

    private void startDbServerUp() {
        try {
            logInfo(LOGGER, this, "Trying to start H2 db server up...");
            dbServer = Server.createTcpServer("-tcpPort", "9092", "-tcpAllowOthers").start(); //9101
            logInfo(LOGGER, this, "H2 db server started up successfully");
            /*Server webServer = Server.createWebServer(
                    "-web", "-webAllowOthers", "-webPort", "8082").start();*/
        } catch (Exception e) {
            if (!(e instanceof JdbcSQLException && e.getCause() instanceof BindException))
                logError(LOGGER, this, e);
            else
                logInfo(LOGGER, this, "H2 db server was already up");
        }
    }

    private void createTablesIfNotExist() {
        try {
            logInfo(LOGGER, this, "Running SQL database scripts...");
            String dbTablesDirPath = "db/tables/";
            String accountTablePath = dbTablesDirPath + "01.account.sql";
            runScript(accountTablePath);
            String fundsTransferTxTablePath = dbTablesDirPath + "02.funds-transfer-tx.sql";
            runScript(fundsTransferTxTablePath);
            logInfo(LOGGER, this, "SQL scripts run successfully");
        } catch (Exception e) {
            logError(LOGGER, this, e);
            logInfo(LOGGER, this, "Execution of SQL scripts aborted");
        }
    }

    private void runScript(String scriptFilePath) throws IOException, SQLException {
        InputStream sqlScriptInputStream =
                currentThread().getContextClassLoader().getResourceAsStream(scriptFilePath);
        if (sqlScriptInputStream == null)
            throw new RuntimeException("SQL script file not found: " + scriptFilePath);
        String createTableQuery =
                new String(IOUtils.toByteArray(sqlScriptInputStream), "UTF-8");
        /*DataSource dataSource = (DataSource)
                new InitialContext().lookup("java:/jdbc/account-management-data-source");*/
        dataSource.getConnection().createStatement().executeUpdate(createTableQuery);
    }
}
