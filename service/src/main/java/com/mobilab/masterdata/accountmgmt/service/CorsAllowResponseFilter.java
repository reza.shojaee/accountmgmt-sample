package com.mobilab.masterdata.accountmgmt.service;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;

import static com.google.common.net.HttpHeaders.*;
import static javax.ws.rs.HttpMethod.*;

/**
 * @author Reza Shojaee, 11/16/18 3:28 PM
 */
@Provider
public class CorsAllowResponseFilter implements ContainerResponseFilter {
    private static final String HEADER_VALUES_SEPARATOR = ", ";

    @Override
    public void filter(
            ContainerRequestContext containerRequestContext,
            ContainerResponseContext containerResponseContext) {
        MultivaluedMap<String, Object> headers = containerResponseContext.getHeaders();
        if (!headers.containsKey(ACCESS_CONTROL_ALLOW_ORIGIN))
            headers.add(ACCESS_CONTROL_ALLOW_ORIGIN, "*");
        if (!headers.containsKey(ACCESS_CONTROL_ALLOW_METHODS))
            headers.add(ACCESS_CONTROL_ALLOW_METHODS, GET + HEADER_VALUES_SEPARATOR
                    + POST + HEADER_VALUES_SEPARATOR
                    + PUT + HEADER_VALUES_SEPARATOR
                    + PATCH.NAME + HEADER_VALUES_SEPARATOR
                    + DELETE + HEADER_VALUES_SEPARATOR
                    + HEAD + HEADER_VALUES_SEPARATOR
                    + OPTIONS + HEADER_VALUES_SEPARATOR
                    + "CONNECT" + HEADER_VALUES_SEPARATOR
                    + "TRACE");
        if (!headers.containsKey(ACCESS_CONTROL_ALLOW_HEADERS))
            headers.add(ACCESS_CONTROL_ALLOW_HEADERS, ACCEPT + HEADER_VALUES_SEPARATOR
                    + ACCESS_CONTROL_ALLOW_ORIGIN + HEADER_VALUES_SEPARATOR
                    + AUTHORIZATION + HEADER_VALUES_SEPARATOR
                    + CONTENT_TYPE + HEADER_VALUES_SEPARATOR
                    + ORIGIN);
        if (!headers.containsKey(ACCESS_CONTROL_MAX_AGE))
            headers.add(ACCESS_CONTROL_MAX_AGE, 72_000); // 20 hours
    }
}
