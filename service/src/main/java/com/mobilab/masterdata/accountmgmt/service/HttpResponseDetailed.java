package com.mobilab.masterdata.accountmgmt.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.RuntimeJsonMappingException;
import com.mobilab.masterdata.accountmgmt.data.exception.BusinessLogicException;
import com.mobilab.masterdata.accountmgmt.data.exception.ConcurrentUpdatesException;
import com.mobilab.masterdata.accountmgmt.data.exception.NonexistentRecordException;
import com.mobilab.masterdata.accountmgmt.data.exception.SystemException;
import com.mobilab.masterdata.accountmgmt.util.exception.ExceptionContextMixIn;

import javax.ejb.DuplicateKeyException;
import javax.ejb.EJBException;
import javax.persistence.EntityNotFoundException;
import javax.persistence.OptimisticLockException;
import javax.transaction.RollbackException;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.Response.StatusType;
import java.util.*;
import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.Map.Entry;
import java.util.logging.Logger;

import static com.mobilab.masterdata.accountmgmt.data.entity.BaseEntity.LOCK_VERSION_ATTRIBUTE;
import static com.mobilab.masterdata.accountmgmt.data.exception.BusinessLogicException.*;
import static com.mobilab.masterdata.accountmgmt.data.exception.BusinessLogicException.MULTIPLE_ENTITIES_FOR_UNIQUE;
import static com.mobilab.masterdata.accountmgmt.data.exception.BusinessLogicException.NO_UNIQUE_ENTITY;
import static com.mobilab.masterdata.accountmgmt.util.json.JsonUtil.serialize;
import static javax.ws.rs.core.Response.Status.*;

/**
 * @author Reza Shojaee, 11/16/18 3:20 PM
 */
@SuppressWarnings({ "unused", "WeakerAccess" })
public class HttpResponseDetailed {
    private static final Logger LOGGER = Logger.getLogger(HttpResponseDetailed.class.getName());
    public static final String TEMPORARY_ERROR_MESSAGE = "Please try again later and if "
            + "the problem persists, please contact the system administrator";
    public static final Response DATABASE_CONNECTION_PROBLEM_MESSAGE = response(
            INTERNAL_SERVER_ERROR, "1502",
            "Connection to database failed. " + TEMPORARY_ERROR_MESSAGE);
    private static final String CONCURRENT_UPDATES_MESSAGE =
            ConcurrentUpdatesException.class.getSimpleName() + ": "
                    + "Another user has already changed the same record; please first refresh "
                    + "then try the update again!";
    private static final String NONEXISTENT_RECORD_MESSAGE =
            NonexistentRecordException.class.getSimpleName() + ": "
                    + "No such record exists to be updated/deleted!";
    private static final String DUPLICATE_REQUEST_MESSAGE =
            DuplicateKeyException.class.getSimpleName() + ": "
                    + "Duplicate transaction request";

    private static final Set<Class<? extends Throwable>> SYSTEM_EXCEPTIONS = new HashSet<>();
    private static final Set<String> SYSTEM_EXCEPTION_NAMES = new HashSet<>();

    static {
        SYSTEM_EXCEPTIONS.add(IllegalArgumentException.class);
        SYSTEM_EXCEPTIONS.add(JsonProcessingException.class);
        SYSTEM_EXCEPTIONS.add(NullPointerException.class);
        SYSTEM_EXCEPTIONS.add(RuntimeJsonMappingException.class);
        SYSTEM_EXCEPTIONS.add(SystemException.class);
        SYSTEM_EXCEPTION_NAMES.add("SQLGrammarException");
    }

    private HttpResponseDetailed() {}

    public static Response response(StatusType status, Result result) {
        return Response.status(status).entity(serialize(result)).build();
    }

    public static Response response(StatusType status, String errorCode, String message) {
        return response(status, new Result<>(errorCode, message));
    }

    public static Response response(StatusType status, String message) {
        return response(status, new Result<>(message));
    }

    public static Response response(StatusType status, Object result) {
        return response(status, result instanceof Result ? (Result) result : new Result<>(result));
    }

    public static Response ok(Object result) {
        return response(OK, result);
    }

    public static Response ok(List<?> results) {
        return response(OK, new Result<>(results));
    }

    public static Response ok() {
        return response(OK, (Result) null);
    }

    public static Response badRequest(String message) {
        return response(BAD_REQUEST, message);
    }

    public static Response badRequest(Object result) {
        return response(BAD_REQUEST, result);
    }

    public static Response unauthorized(
            String errorCode, String authenticationScheme, String authenticationRealm,
            String message, String... messageArgs) {
        String formattedMessage = message;
        if (messageArgs.length > 0)
            formattedMessage = String.format(message, (Object[]) messageArgs);
        return Response.status(UNAUTHORIZED)
                .header(HttpHeaders.WWW_AUTHENTICATE, authenticationScheme
                        + " realm=\"" + authenticationRealm + "\"")
                .entity(serialize(new Result<Void>(errorCode, formattedMessage))).build();
    }

    public static StatusType newStatus(int statusCode, Family family, String reason) {
        return new StatusType() {
            @Override
            public int getStatusCode() {
                return statusCode;
            }

            @Override
            public Family getFamily() {
                return family;
            }

            @Override
            public String getReasonPhrase() {
                return reason;
            }
        };
    }

    public static Entry<StatusType, String> mapErrorCode(Throwable throwable) {
        StatusType resultStatus = Status.BAD_REQUEST;
        String responseEntity = null;
        if (throwable instanceof ConcurrentUpdatesException) {
            resultStatus = Status.CONFLICT;
            responseEntity = CONCURRENT_UPDATES_MESSAGE;
        } else if (throwable instanceof NonexistentRecordException) {
            resultStatus = Status.NOT_FOUND;
            responseEntity = NONEXISTENT_RECORD_MESSAGE;
        } else if (throwable instanceof BusinessLogicException) {
            List<String> errorFields = ((ExceptionContextMixIn<?>) throwable).getErrorFields();
            List<String> errorConditions =
                    ((ExceptionContextMixIn<?>) throwable).getErrorConditions();
            if (errorFields.contains(LOCK_VERSION_ATTRIBUTE))
                resultStatus = newStatus(428, Status.Family.CLIENT_ERROR, "PRECONDITION_REQUIRED");
            else if (errorConditions.contains(UPSTREAM_ERROR_RESPONSE))
                resultStatus = Status.BAD_GATEWAY;
            else if (errorConditions.contains(UPSTREAM_RESPONSE_TIMEOUT))
                resultStatus = Status.BAD_GATEWAY;
            else if (errorConditions.contains(UPSTREAM_CONNECT_FAILED))
                resultStatus = Status.BAD_GATEWAY;
            else if (errorConditions.contains(UNIQUE_ENTITY)
                    || errorConditions.contains(NO_UNIQUE_ENTITY)
                    || errorConditions.contains(MULTIPLE_ENTITIES_FOR_UNIQUE))
                resultStatus = Status.EXPECTATION_FAILED;
        } else if (throwable instanceof EJBException) {
            if (throwable.getCause() instanceof OptimisticLockException) {
                resultStatus = Status.CONFLICT;
                responseEntity = CONCURRENT_UPDATES_MESSAGE;
            } else if (throwable.getCause() instanceof EntityNotFoundException) {
                resultStatus = Status.NOT_FOUND;
                responseEntity = NONEXISTENT_RECORD_MESSAGE;
            } else if (throwable.getCause() instanceof RollbackException) {
                boolean uniqueConstraint = false;
                Throwable cause = throwable.getCause().getCause();
                while (cause != null) {
                    String causeClassName = cause.getClass().getSimpleName();
                    // Don't compare class objects, because of different packages of these exceptions
                    if (causeClassName.equals("ConstraintViolationException")
                            || causeClassName.equals("SQLIntegrityConstraintViolationException")) {
                        uniqueConstraint = true;
                        break;
                    }
                    cause = cause.getCause();
                }
                if (uniqueConstraint) {
                    resultStatus = Status.CONFLICT;
                    responseEntity = DUPLICATE_REQUEST_MESSAGE;
                } else {
                    resultStatus = Status.INTERNAL_SERVER_ERROR;
                }
            } else {
                resultStatus = Status.INTERNAL_SERVER_ERROR;
            }
        } else if (throwable instanceof SystemException) {
            resultStatus = Status.INTERNAL_SERVER_ERROR;
            responseEntity = "An unexpected system error occurred, please report this incident "
                    + "to system administrator.";
        } else if (SYSTEM_EXCEPTIONS.contains(throwable.getClass())
                || SYSTEM_EXCEPTION_NAMES.contains(throwable.getClass().getSimpleName())) {
            resultStatus = Status.INTERNAL_SERVER_ERROR;
            responseEntity = "A temporary system error occurred which we try to fix it soon. "
                    + TEMPORARY_ERROR_MESSAGE;
        }
        return new SimpleImmutableEntry<>(resultStatus, responseEntity);
    }
}
