package com.mobilab.masterdata.accountmgmt.service;

import com.mobilab.masterdata.accountmgmt.business.AccountManager;
import com.mobilab.masterdata.accountmgmt.business.FundsTransferTxManager;
import com.mobilab.masterdata.accountmgmt.data.dto.FundsTransferTxDto;
import com.mobilab.masterdata.accountmgmt.data.entity.Account;
import com.mobilab.masterdata.accountmgmt.data.entity.FundsTransferTx;
import com.mobilab.masterdata.accountmgmt.data.exception.ValidationException;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.util.Calendar;

import static com.mobilab.masterdata.accountmgmt.service.HttpResponseDetailed.ok;
import static javax.ws.rs.core.Response.Status.OK;

/**
 * @author Reza Shojaee, 11/16/18 4:05 PM
 */
@Path("funds-transfer-tx")
public class FundsTransferTxService extends BaseService<FundsTransferTx, FundsTransferTxDto> {
    private static final String DETAIL_CODE_SUCCESSFUL = "00";
    @Inject
    private FundsTransferTxManager fundsTransferTxManager;
    @Inject
    private AccountManager accountManager;

    public FundsTransferTxService() {
        super(FundsTransferTx.class, FundsTransferTxDto.class);
    }

    @POST
    @Path("")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response create(FundsTransferTxDto fundsTransferTx) {
        return execute(requestReference -> {
            if (fundsTransferTx.getSourceAccount() == null 
                    || fundsTransferTx.getDestinationAccount() == null)
                throw new ValidationException("Source or destination account info missing!");
            Account sourceAccount;
            if (fundsTransferTx.getSourceAccount().getId() != null)
                sourceAccount =
                        accountManager.retrieve(fundsTransferTx.getSourceAccount().getId());
            else if (fundsTransferTx.getSourceAccount().getAccountNo() != null)
                sourceAccount = accountManager.retrieveByAccountNo(
                        fundsTransferTx.getSourceAccount().getAccountNo());
            else
                throw new ValidationException("Either source account id or name must be given");
            fundsTransferTx.setSourceAccount(sourceAccount);
            Account destinationAccount;
            if (fundsTransferTx.getDestinationAccount().getId() != null)
                destinationAccount =
                        accountManager.retrieve(fundsTransferTx.getDestinationAccount().getId());
            else if (fundsTransferTx.getDestinationAccount().getAccountNo() != null)
                destinationAccount = accountManager.retrieveByAccountNo(
                        fundsTransferTx.getDestinationAccount().getAccountNo());
            else
                throw new ValidationException(
                        "Either destination account id or name must be given");
            fundsTransferTx.setDestinationAccount(destinationAccount);
            fundsTransferTx.setStoreTimestamp(Calendar.getInstance());
            FundsTransferTx request = fundsTransferTxManager.store(toEntity(fundsTransferTx));
            requestReference.set(request);
            FundsTransferTx response = fundsTransferTxManager.doTransaction(request);
            Result<FundsTransferTx> result =
                    new Result<>(DETAIL_CODE_SUCCESSFUL, "Successful", response);
            response.setResponseCode("" + OK.getStatusCode());
            response.setDetailCode(result.getDetailCode());
            FundsTransferTx updateCopy = copyForUpdate(response);
            fundsTransferTxManager.patch(updateCopy);
            response.setResponseCode(null);
            response.setDetailCode(null);
            return ok(result);
        }, (request, response) -> {
            request.setResponseTimestamp(Calendar.getInstance());
            request.setResponseCode("" + response.getStatus());
            fundsTransferTxManager.patch(copyForUpdate(request));
            request.setResponseCode(null);
        }, fundsTransferTx);
    }

    @Path("")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response list() {
        return super.list();
    }

    @Path("{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response find(@PathParam("id") long id) {
        return super.find(id);
    }
    
    private FundsTransferTx copyForUpdate(FundsTransferTx fundsTransferTx) {
        FundsTransferTx toBeUpdated = new FundsTransferTx();
        toBeUpdated.setId(fundsTransferTx.getId());
        toBeUpdated.setExchangeRate(fundsTransferTx.getExchangeRate());
        toBeUpdated.setResponseCode(fundsTransferTx.getResponseCode());
        toBeUpdated.setDetailCode(fundsTransferTx.getDetailCode());
        toBeUpdated.setResponseTimestamp(Calendar.getInstance());
        toBeUpdated.setLockVersion(1);
        toBeUpdated.setRequestTimestamp(null);
        toBeUpdated.setStoreTimestamp(null);
        return toBeUpdated;
    }

    @Override
    public FundsTransferTxManager getBusinessManager() {
        return fundsTransferTxManager;
    }
}
