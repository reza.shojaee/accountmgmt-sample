package com.mobilab.masterdata.accountmgmt.service;

import com.mobilab.masterdata.accountmgmt.business.BusinessManager;
import com.mobilab.masterdata.accountmgmt.data.entity.BaseEntity;
import com.mobilab.masterdata.accountmgmt.data.exception.ValidationException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.*;
import javax.ws.rs.core.Response.StatusType;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static com.mobilab.masterdata.accountmgmt.service.HttpResponseDetailed.ok;
import static com.mobilab.masterdata.accountmgmt.service.HttpResponseDetailed.response;
import static com.mobilab.masterdata.accountmgmt.util.exception.ExceptionUtil.makeConciseExceptionInfo;
import static com.mobilab.masterdata.accountmgmt.util.json.JsonUtil.serialize;
import static com.mobilab.masterdata.accountmgmt.util.logging.LogUtil.logError;
import static com.mobilab.masterdata.accountmgmt.util.logging.LogUtil.logInfo;
import static com.mobilab.masterdata.accountmgmt.util.reflect.ReflectionUtil.setCorrespondingProperties;

/**
 * @author Reza Shojaee, 11/16/18 3:43 PM
 */
@SuppressWarnings({ "unused", "WeakerAccess", "unchecked" })
public abstract class BaseService<T extends BaseEntity, U extends T> {
    protected static final String ID_KEY = "id";
    private static final String LOG_REQUEST_PREFIX = "Request: ";
    private static final String LOG_RESPONSE_PREFIX = "Response: ";
    private static final int MASK_COARSE_THRESHOLD = 500;
    private static final int MASK_COARSE_PARTS_LENGTH = 100;

    // NB Don't define a static logger, for subclasses to resolve their own specific logger
    protected Logger logger = Logger.getLogger(this.getClass().getName());
    private Class<T> entityClass;
    private Class<U> dtoClass;

    @Context
    protected UriInfo uriInfo;
    @Context
    protected ResourceInfo dispatchedMethodInfo;
    @Context
    protected HttpHeaders httpHeaders;
    @Context
    protected Request request;
    @Context
    protected HttpServletRequest servletRequest;
    @Context
    protected HttpServletResponse servletResponse;
    @Context
    protected ServletConfig servletConfig;
    @Context
    protected ServletContext servletContext;
    @Context
    protected SecurityContext securityContext;
    @Context
    protected ContainerRequestContext containerRequestContext;

    public BaseService(Class<T> entityClass, Class<U> dtoClass) {
        this.entityClass = entityClass;
        this.dtoClass = dtoClass;
    }

    public Response create(U resource) {
        return execute(() -> ok(toDto(
                (T) getBusinessManager().store(toEntity(resource)))
        ), resource);
    }

    public Response list() {
        return execute(() -> ok(toDto(
                (List<T>) getBusinessManager().retrieveAll()))
        );
    }

    public Response find(long id) {
        return execute(() -> ok(toDto(
                (T) getBusinessManager().retrieve(id))
        ), id);
    }

    public Response update(U resource) {
        return execute(() -> ok(toDto(
                (T) getBusinessManager().update(toEntity(resource)))
        ), resource);
    }

    public Response patch(U resource) {
        return execute(() -> ok(toDto(
                (T) getBusinessManager().patch(toEntity(resource)))
        ), resource);
    }

    public Response delete(long id) {
        return execute(() -> {
            getBusinessManager().delete(id);
            return ok();
        }, id);
    }

    public Response execute(Callable<Response> codeBlock, Object... inputs) {
        try {
            Map<String, Object> requestLogItems = makeRequestItems(inputs);
            logInfo(logger, this, LOG_REQUEST_PREFIX + serialize(requestLogItems));
            Response response = codeBlock.call();
            Map<String, Object> responseLogItems = makeResponseItems(response);
            logInfo(logger, this, LOG_RESPONSE_PREFIX + serialize(responseLogItems));
            return response;
        } catch (Exception e) {
            String responseEntity = makeConciseExceptionInfo(e);
            logError(logger, this, responseEntity, e);
            Entry<StatusType, String> errorData = mapErrorCode(e);
            if (errorData.getValue() != null)
                responseEntity = errorData.getValue();
            Response response = response(errorData.getKey(), responseEntity);
            logInfo(logger, this, LOG_RESPONSE_PREFIX + serialize(makeResponseItems(response)));
            return response;
        }
    }

    public Response execute(
            Function<AtomicReference<T>, Response> codeBlock, BiConsumer<T, Response> errorHandler,
            Object... inputs) {
        AtomicReference<T> requestReference = new AtomicReference<>();
        try {
            Map<String, Object> requestLogItems = makeRequestItems(inputs);
            logInfo(logger, this, LOG_REQUEST_PREFIX + serialize(requestLogItems));
            Response response = codeBlock.apply(requestReference);
            logInfo(logger, this, LOG_RESPONSE_PREFIX + serialize(makeResponseItems(response)));
            return response;
        } catch (Exception e) {
            String responseEntity = makeConciseExceptionInfo(e);
            logError(logger, this, responseEntity, e);
            Entry<StatusType, String> errorData = mapErrorCode(e);
            if (errorData.getValue() != null)
                responseEntity = errorData.getValue();
            Response response = response(errorData.getKey(), responseEntity);
            logInfo(logger, this, LOG_RESPONSE_PREFIX + serialize(makeResponseItems(response)));
            //noinspection finally
            try {
                errorHandler.accept(requestReference.get(), response);
            } finally {
                //noinspection ReturnInsideFinallyBlock
                return response;
            }
        }
    }

    // This instance method is here to give a chance to subclasses to override it if necessary
    protected Entry<StatusType, String> mapErrorCode(Throwable throwable) {
        return HttpResponseDetailed.mapErrorCode(throwable);
    }

    public void verifyUpdateId(U dto, long id) {
        Long entityId = dto.getId();
        if (entityId == null) {
            dto.setId(id);
        } else {
            if (entityId != id)
                throw new ValidationException("Same resource ID expected in body object and URL "
                        + "while two different IDs specified; which one do you intend to update, "
                        + entityId + " (in URL) or " + id + " (in body)?")
                        .errorField("id");
        }
    }

    public T toEntity(U dto) {
        if (dto == null)
            return null;
        try {
            T entity = entityClass.getConstructor().newInstance();
            setCorrespondingProperties(dto, entity);
            return entity;
        } catch (Exception e) {
            return dto;
        }
    }

    public U toDto(T entity, boolean withRelations) {
        if (entity == null)
            return null;
        try {
            return dtoClass.getConstructor(entity.getClass()).newInstance(entity);
        } catch (Exception e) {
            return (U) entity;
        }
    }

    public U toDto(T entity, String... relationsToLoad) {
        return toDto(entity, relationsToLoad.length > 0);
    }

    public List<T> toDto(List<T> entities, boolean withRelations) {
        return entities.stream().map(entity -> toDto(entity, withRelations))
                .filter(Objects::nonNull).collect(Collectors.toList());
    }

    public List<T> toDto(List<T> entities, String... relationsToLoad) {
        return toDto(entities, relationsToLoad.length > 0);
    }

    protected abstract BusinessManager<? super T> getBusinessManager();

    protected String getRequesterAddress() {
        return servletRequest.getRemoteHost() + ":" + servletRequest.getRemotePort();
    }

    protected String getLocalEndpointAddress() {
        return servletRequest.getLocalAddr() + ":" + servletRequest.getLocalPort();
    }

    private Map<String, Object> makeRequestItems(Object... inputs) {
        Map<String, Object> items = new LinkedHashMap<>();
        items.put("requester", getRequesterAddress());
        items.put("uri", uriInfo.getAbsolutePath());
        items.put("method", servletRequest.getMethod());
        /*items.put("serverInfo", servletRequest.getServerName() + ":"
                + servletRequest.getServerPort());*/
        items.put("inputs", inputs.length == 0 || inputs.length == 1 && inputs[0] == null
                ? "" : serialize(inputs));
        if (Level.FINE == logger.getLevel()) {
            items.put("httpMethod", servletRequest.getMethod());
            items.put("securityContext", securityContext.getAuthenticationScheme()
                    + ";" + securityContext.getUserPrincipal());
            items.put("httpHeaders", serialize(httpHeaders));
            items.put("dispatchedMethod", dispatchedMethodInfo.getResourceMethod());
        }
        return items;
    }

    private Map<String, Object> makeResponseItems(Response response) {
        Map<String, Object> items = new LinkedHashMap<>();
        items.put("status", response.getStatusInfo());
        items.put("statusCode", response.getStatus());
        String entity = makeAppropriateLogContent(response);
        items.put("entity", cutShort(entity));
        items.put("entityLength", entity.length());
        items.put("receiver", getRequesterAddress());
        return items;
    }

    private String makeAppropriateLogContent(Response response) {
        Object entity = response.getEntity();
        if (entity == null)
            return "<NULL>";
        if (!(entity instanceof Result))
            return entity.toString();
        Result result = (Result) entity;
        return result.toString();
    }

    private static String cutShort(String message) {
        if (message.length() <= MASK_COARSE_THRESHOLD)
            return message;
        int middleOfMessageIndex = message.length() / 2;
        return message.substring(0, MASK_COARSE_PARTS_LENGTH) + "*****"
                + message.substring(middleOfMessageIndex - MASK_COARSE_PARTS_LENGTH,
                middleOfMessageIndex + MASK_COARSE_PARTS_LENGTH)
                + "*****" + message.substring(message.length() - MASK_COARSE_PARTS_LENGTH);
    }
}
