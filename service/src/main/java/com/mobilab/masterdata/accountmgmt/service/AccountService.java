package com.mobilab.masterdata.accountmgmt.service;

import com.mobilab.masterdata.accountmgmt.business.AccountManager;
import com.mobilab.masterdata.accountmgmt.data.dto.AccountDto;
import com.mobilab.masterdata.accountmgmt.data.entity.Account;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static com.mobilab.masterdata.accountmgmt.service.HttpResponseDetailed.ok;

/**
 * @author Reza Shojaee, 11/16/18 4:02 PM
 */
@Path("account")
public class AccountService extends BaseService<Account, AccountDto> {
    @Inject
    private AccountManager accountManager;

    public AccountService() {
        super(Account.class, AccountDto.class);
    }

    @POST
    @Path("")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response create(AccountDto account) {
        return super.create(account);
    }

    @Path("")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response list() {
        return super.list();
    }

    @Path("{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response find(@PathParam("id") long id) {
        return super.find(id);
    }

    @Path("{id}")
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response update(AccountDto account, @PathParam("id") long id) {
        return execute(() -> {
            verifyUpdateId(account, id);
            return ok(toDto(getBusinessManager().update(toEntity(account))));
        }, account, id);
    }

    @Path("{id}")
    @PATCH
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response patch(AccountDto account, @PathParam("id") long id) {
        return execute(() -> {
            verifyUpdateId(account, id);
            getBusinessManager().patch(toEntity(account));
            // return full view of the current state of the patched resource
            return ok(toDto(getBusinessManager().retrieve(account.getId())));
        }, account, id);
    }

    @Path("{id}")
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("id") long id) {
        return super.delete(id);
    }

    @Override
    public AccountManager getBusinessManager() {
        return accountManager;
    }
}
