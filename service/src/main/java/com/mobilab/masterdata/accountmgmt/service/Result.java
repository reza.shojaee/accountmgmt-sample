package com.mobilab.masterdata.accountmgmt.service;

import com.mobilab.masterdata.accountmgmt.data.entity.BaseEntity;

import java.util.List;

/**
 * @author Reza Shojaee, 11/16/18 3:30 PM
 */
@SuppressWarnings({ "WeakerAccess", "unused" })
public class Result<T> extends BaseEntity {
    private String detailCode;
    private String detailMessage;
    private T resource;
    private List<T> resources;

    public Result(String detailCode, String detailMessage, T resource) {
        this.detailCode = detailCode;
        this.detailMessage = detailMessage;
        this.resource = resource;
    }

    public Result(String detailCode, String detailMessage, List<T> resources) {
        this.detailCode = detailCode;
        this.detailMessage = detailMessage;
        this.resources = resources;
    }

    public Result(String detailCode, String detailMessage) {
        this(detailCode, detailMessage, (T) null);
    }

    public Result(String detailMessage) {
        this(null, detailMessage, (T) null);
    }

    public Result(T resource) {
        this(null, null, resource);
    }

    public Result(List<T> resources) {
        this(null, null, resources);
    }

    public Result() {}

    public String getDetailCode() {
        return detailCode;
    }

    public void setDetailCode(String detailCode) {
        this.detailCode = detailCode;
    }

    public String getDetailMessage() {
        return detailMessage;
    }

    public void setDetailMessage(String detailMessage) {
        this.detailMessage = detailMessage;
    }

    public T getResource() {
        return resource;
    }

    public void setResource(T resource) {
        this.resource = resource;
    }

    public List<T> getResources() {
        return resources;
    }

    public void setResources(List<T> resources) {
        this.resources = resources;
    }
}
