package com.mobilab.masterdata.accountmgmt.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.mobilab.masterdata.accountmgmt.data.dto.AccountDto;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.StatusType;
import java.math.BigDecimal;
import java.net.URI;
import java.net.URL;
import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.Arrays;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import static com.mobilab.masterdata.accountmgmt.util.json.JsonUtil.deserialize;
import static com.mobilab.masterdata.accountmgmt.util.json.JsonUtil.serialize;
import static com.mobilab.masterdata.accountmgmt.util.reflect.ReflectionUtil.findPropertiesValues;
import static com.mobilab.masterdata.accountmgmt.util.reflect.ReflectionUtil.getPropertyValue;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * @author Reza Shojaee, 11/16/18 4:49 PM
 */
@RunWith(Arquillian.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AccountServiceTest {
    @ArquillianResource
    private URL contextPath;

    private static AccountDto account; // shared instance between test cases, mind order!

    @BeforeClass
    public static void setUp() {
        account = new AccountDto();
        account.setAccountNo("1234567890");
        account.setCurrencyCode("EUR");
        account.setCurrentBalance(new BigDecimal("1000.45"));
        account.setOwnerName("John Doe");
        account.setRemarks("Hi there");
    }

    @Deployment(testable = false)
    @SuppressWarnings("unused")
    public static WebArchive createArtifact() {
        List<String> dependencyArtifacts = Arrays.asList(
                "com.mobilab.masterdata.accountmgmt:util:1.0-SNAPSHOT",
                "com.mobilab.masterdata.accountmgmt:data:1.0-SNAPSHOT",
                "com.mobilab.masterdata.accountmgmt:business:1.0-SNAPSHOT",
                "com.h2database:h2:1.4.197");
        WebArchive webArchive = ShrinkWrap.create(WebArchive.class);
        dependencyArtifacts.forEach(dependency ->
                webArchive.addAsLibraries(Maven.resolver().loadPomFromFile("pom.xml")
                        .importCompileAndRuntimeDependencies()
                        .resolve(dependency) // no version please :)
                        .withoutTransitivity()
                        .as(JavaArchive.class)));
        return webArchive
                //.addClasses(AccountService.class, AccountManager.class)
                //.addPackages(true, "com.mobilab.masterdata.accountmgmt.util")
                //.addPackages(true, "com.mobilab.masterdata.accountmgmt.data")
                //.addPackages(true, AccountManager.class.getPackage())
                .addPackages(true, AccountService.class.getPackage())
                //.addPackages(true, "org.h2")
                //.addPackages(true, Server.class.getPackage()) // H2 tools dependency
                /*.addAsLibraries(Maven.resolver().loadPomFromFile("pom.xml")
                        .importCompileAndRuntimeDependencies()
                        .resolve("com.h2database:h2") // no version please :)
                        .withoutTransitivity()
                        .as(JavaArchive.class))*/
                //.addAsResource("META-INF/persistence.xml")
                //.addAsWebInfResource(new File("src/main/resources/META-INF/beans.xml"));
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
        /*EnterpriseArchive enterpriseArchive = Maven.configureResolver().workOffline()
                .resolve("com.mobilab.masterdata.accountmgmt:application:1.0-SNAPSHOT")
                .withoutTransitivity().asSingle(EnterpriseArchive.class);
        enterpriseArchive.writeTo(System.out, Formatters.VERBOSE);
        return enterpriseArchive;*/
    }

    @Test
    public void aCreate() throws Exception {
        Entry<StatusType, Result<AccountDto>> result = doCreate(account);
        assertThat(result.getKey().getStatusCode(), is(equalTo(200)));
        AccountDto responseAccount = result.getValue().getResource();
        assertThat(responseAccount, is(not(nullValue())));
        assertThat(responseAccount, is(not(nullValue())));
        assertThat(responseAccount.getId(), is(not(nullValue())));
        assertThat(responseAccount.getLockVersion(), is(not(nullValue())));
        assertThat(responseAccount.getTimestamp(), is(not(nullValue())));
        account.setId(responseAccount.getId());
        account.setLockVersion(responseAccount.getLockVersion());
        account.setTimestamp(responseAccount.getTimestamp());
        makeSureTheyAreEqual(account, responseAccount);
        eList();
    }

    @Test
    public void bUpdate() throws Exception {
        Entry<StatusType, Result<AccountDto>> result = doUpdate(account);
        assertThat(result.getKey().getStatusCode(), is(equalTo(200)));
        AccountDto foundAccount = result.getValue().getResource();
        assertThat(foundAccount, is(not(nullValue())));
        account.setLockVersion(account.getLockVersion() + 1);
        account.setTimestamp(null); // FIXME problem because of difference in time zone, leave it for now
        makeSureTheyAreEqual(account, foundAccount);
    }

    @Test
    public void cPatch() throws Exception {
        account.setId(account.getId());
        Entry<StatusType, Result<AccountDto>> result = doPatch(account);
        assertThat(result.getKey().getStatusCode(), is(equalTo(200)));
        AccountDto foundAccount = result.getValue().getResource();
        assertThat(foundAccount, is(not(nullValue())));
        account.setLockVersion(account.getLockVersion() + 1);
        makeSureTheyAreEqual(account, foundAccount);
    }

    @Test
    public void dFind() throws Exception {
        Entry<StatusType, Result<AccountDto>> result = doFind(account.getId());
        assertThat(result.getKey().getStatusCode(), is(equalTo(200)));
        AccountDto foundAccount = result.getValue().getResource();
        assertThat(foundAccount, is(not(nullValue())));
        makeSureTheyAreEqual(account, foundAccount);
    }

    @Test
    public void eList() throws Exception {
        Entry<StatusType, Result<AccountDto>> result = doList();
        assertThat(result.getKey().getStatusCode(), is(equalTo(200)));
        List<AccountDto> accountList = result.getValue().getResources();
        assertThat(accountList, is(not(nullValue())));
        AccountDto listedAccount = accountList.stream()
                .filter(listAccount -> listAccount.getId().equals(account.getId())).findFirst()
                .orElseThrow(() -> new Exception("Account had not been created!"));
        assertThat(listedAccount.getTimestamp(), is(not(nullValue())));
        makeSureTheyAreEqual(account, listedAccount);
    }

    @Test
    public void fDelete() throws Exception {
        //account.setId(117001L);
        Response response = doDelete(account.getId());
        assertThat(response.getStatus(), is(equalTo(200)));
    }

    private WebTarget buildClient(String url) throws Exception {
        // Apparently the timeout values below should be read from config
        int connectTimeout = 4;
        int responseTimeout = 16;
        // +JAX-RS 2.1: ClientBuilder.newBuilder().connectTimeout(connectTimeout, TimeUnit.SECONDS)
        // .readTimeout(responseTimeout, TimeUnit.SECONDS).build();
        Client client = new ResteasyClientBuilder()
                .establishConnectionTimeout(connectTimeout, TimeUnit.SECONDS)
                .socketTimeout(responseTimeout, TimeUnit.SECONDS).build();
        return client.target(new URI(url));
    }

    private Entry<StatusType, Result<AccountDto>> doCreate(AccountDto account) throws Exception {
        Response response = buildClient(contextPath.toString() + "v1/account")
                .request(APPLICATION_JSON_TYPE)
                .post(Entity.entity(serialize(account), APPLICATION_JSON_TYPE));
        String rawResponse = response.readEntity(String.class);
        return new SimpleImmutableEntry<>(response.getStatusInfo(),
                deserialize(rawResponse, new TypeReference<Result<AccountDto>>() {}));
    }

    private Entry<StatusType, Result<AccountDto>> doList() throws Exception {
        Response response = buildClient(contextPath.toString() + "v1/account").request().get();
        String rawResponse = response.readEntity(String.class);
        return new SimpleImmutableEntry<>(response.getStatusInfo(),
                deserialize(rawResponse, new TypeReference<Result<AccountDto>>() {}));
    }

    private Entry<StatusType, Result<AccountDto>> doFind(
            @SuppressWarnings("SameParameterValue") long id)
            throws Exception {
        Response response = buildClient(contextPath.toString() + "v1/account/" + id)
                .request().get();
        String rawResponse = response.readEntity(String.class);
        return new SimpleImmutableEntry<>(response.getStatusInfo(),
                deserialize(rawResponse, new TypeReference<Result<AccountDto>>() {}));
    }

    private Entry<StatusType, Result<AccountDto>> doUpdate(AccountDto account) throws Exception {
        Response response = buildClient(contextPath.toString() + "v1/account/" + account.getId())
                .request(APPLICATION_JSON_TYPE)
                .put(Entity.entity(serialize(account), APPLICATION_JSON_TYPE));
        String rawResponse = response.readEntity(String.class);
        return new SimpleImmutableEntry<>(response.getStatusInfo(),
                deserialize(rawResponse, new TypeReference<Result<AccountDto>>() {}));
    }

    private Entry<StatusType, Result<AccountDto>> doPatch(AccountDto account) throws Exception {
        Response response = buildClient(contextPath.toString() + "v1/account/" + account.getId())
                .request(APPLICATION_JSON_TYPE)
                .method("PATCH", Entity.entity(serialize(account), APPLICATION_JSON_TYPE));
        String rawResponse = response.readEntity(String.class);
        return new SimpleImmutableEntry<>(response.getStatusInfo(),
                deserialize(rawResponse, new TypeReference<Result<AccountDto>>() {}));
    }

    private Response doDelete(long id) throws Exception {
        return buildClient(contextPath.toString() + "v1/account/" + id).request().delete();
    }

    private void makeSureTheyAreEqual(AccountDto sourceAccount, AccountDto targetAccount) {
        findPropertiesValues(sourceAccount).forEach((property, value) -> {
            Object responseValue = getPropertyValue(targetAccount, property);
            if (value != null)
                assertThat(responseValue, is(equalTo(value)));
            /*else
                assertThat(responseValue, is(nullValue()));*/
        });
    }
}
