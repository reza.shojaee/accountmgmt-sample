package com.mobilab.masterdata.accountmgmt.business;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;

/**
 * @author Reza Shojaee, 11/16/18 8:01 PM
 */
@Startup
@Singleton
public class StartupTasksManager {
    @PostConstruct
    void atStartup() {}

    @PreDestroy
    void atShutdown() {}
}
