package com.mobilab.masterdata.accountmgmt.business;

import com.mobilab.masterdata.accountmgmt.data.dao.Dao;
import com.mobilab.masterdata.accountmgmt.data.dao.FundsTransferTxDao;
import com.mobilab.masterdata.accountmgmt.data.entity.Account;
import com.mobilab.masterdata.accountmgmt.data.entity.FundsTransferTx;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.logging.Logger;

import static com.mobilab.masterdata.accountmgmt.util.logging.LogUtil.logDebug;
import static com.mobilab.masterdata.accountmgmt.util.logging.LogUtil.logInfo;

/**
 * @author Reza Shojaee, 11/16/18 4:30 AM
 */
@Stateless
public class FundsTransferTxManager extends BaseBusinessManager<FundsTransferTx> {
    private static final Logger LOGGER = Logger.getLogger(FundsTransferTxManager.class.getName());

    @Inject
    private FundsTransferTxDao dao;
    @Inject
    private AccountManager accountManager;
    @Inject
    private ExchangeRateHandler exchangeRateHandler;

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public FundsTransferTx doTransaction(FundsTransferTx fundsTransferTx) {
        Account sourceAccount = fundsTransferTx.getSourceAccount();
        Account destinationAccount = fundsTransferTx.getDestinationAccount();
        // Optimize flow by first making sure the exchange rate is available and then do withdrawal
        BigDecimal exchangeRate = exchangeRateHandler.getExchangeRate(
                sourceAccount.getCurrencyCode(), destinationAccount.getCurrencyCode());
        exchangeRate = exchangeRate.setScale(4, BigDecimal.ROUND_HALF_DOWN);
        fundsTransferTx.setExchangeRate(exchangeRate);
        logDebug(LOGGER, FundsTransferTxManager.class,
                String.format("Withdrawal operation from account number %s with amount %s%s",
                        sourceAccount.getAccountNo(), fundsTransferTx.getAmount(),
                        sourceAccount.getCurrencyCode()));
        accountManager.withdraw(fundsTransferTx.getSourceAccount(), fundsTransferTx.getAmount());
        BigDecimal depositAmount = fundsTransferTx.getAmount().multiply(exchangeRate);
        depositAmount = depositAmount.setScale(2, BigDecimal.ROUND_HALF_DOWN);
        logInfo(LOGGER, FundsTransferTxManager.class,
                String.format("Deposit operation to account number %s with amount %s%s",
                        destinationAccount.getAccountNo(), depositAmount,
                        destinationAccount.getCurrencyCode()));
        accountManager.deposit(fundsTransferTx.getDestinationAccount(), depositAmount);
        return fundsTransferTx;
    }

    @Override
    public Dao<FundsTransferTx> getDao() {
        return dao;
    }
}
