package com.mobilab.masterdata.accountmgmt.business;

import com.mobilab.masterdata.accountmgmt.data.exception.BusinessLogicException;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.URI;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import static com.mobilab.masterdata.accountmgmt.data.exception.BusinessLogicException.*;
import static com.mobilab.masterdata.accountmgmt.util.logging.LogUtil.logError;
import static com.mobilab.masterdata.accountmgmt.util.logging.LogUtil.logInfo;

/**
 * @author Reza Shojaee, 11/16/18 9:08 AM
 */
public class ExchangeRateHandler {
    private static final Logger LOGGER = Logger.getLogger(ExchangeRateHandler.class.getName());
    private static final BigDecimal SAME_RATE = new BigDecimal("1.00");
    // Of course I would read the base url address from config, anyways for now!
    private static final String API_BASE_URL = "https://api.exchangeratesapi.io/latest";
    /*private static final String API_ACCESS_KEY = "8d6cdbc9f87f92ea971cb604a7224e81";
    private static final String API_BASE_URL =
            "http://data.fixer.io/api/latest?access_key=" + API_ACCESS_KEY;*/

    @SuppressWarnings("WeakerAccess")
    public BigDecimal getExchangeRate(String fromCurrency, String toCurrency) {
        String exchangeRateServiceUrl =
                API_BASE_URL + String.format("?base=%s&symbols=%s", fromCurrency, toCurrency);
        try {
            return callAndGetExchangeRate(exchangeRateServiceUrl, fromCurrency, toCurrency);
        } catch (Exception e) {
            logError(LOGGER, this, e);
            logInfo(LOGGER, this, "Could not get exchange rate from upstream server, " +
                    "returning the value from dummy rates provided for local tests");
            return getExchangeRateForDummyTest(fromCurrency, toCurrency);
        }
    }

    private BigDecimal callAndGetExchangeRate(
            String url, String sourceCurrency, String targetCurrency)
            throws Exception {
        String logMessage = "Upstream request: {url:\"" + url
                + "\",sourceCurrency:\"" + sourceCurrency
                + "\",targetCurrency:\""  + targetCurrency + "\"}";
        logInfo(LOGGER, this, logMessage);
        Response response;
        try {
            response = buildClient(url).request().get();
        } catch (ConnectException e) {
            throw new BusinessLogicException(
                    "Failed to connect to upstream server at " + url, e)
                    .errorCondition(UPSTREAM_CONNECT_FAILED);
        } catch (ProcessingException e) {
            if (e.getCause() instanceof ConnectException)
                throw new BusinessLogicException(
                        "Failed to connect to upstream server at " + url, e)
                        .errorCondition(UPSTREAM_CONNECT_FAILED);
            else if (e.getCause() instanceof SocketTimeoutException)
                throw new BusinessLogicException(
                        "Response timeout form upstream server at " + url, e)
                        .errorCondition(UPSTREAM_RESPONSE_TIMEOUT);
            throw new BusinessLogicException(e);
        }
        logInfo(LOGGER, this, "Upstream status: {url:\"" + url
                + "\",status:\"" + response.getStatus() + "\"}");
        if (Response.Status.OK.getStatusCode() == response.getStatus()) {
            response.bufferEntity();
            String responseEntity = response.readEntity(String.class);
            logInfo(LOGGER, this, "Upstream data: {response:" + responseEntity + "}");
            //noinspection unchecked
            Map<String, Object> upstreamResponseData = response.readEntity(Map.class);
            //noinspection unchecked
            Map<String, BigDecimal> rates =
                    (Map<String, BigDecimal>) upstreamResponseData.get("rates");
            return rates.get(targetCurrency);
        } else {
            throw new BusinessLogicException("Upstream transaction failed with HTTP status code: "
                    + response.getStatus()).errorCondition(UPSTREAM_ERROR_RESPONSE);
        }
    }

    private WebTarget buildClient(String url) throws Exception {
        // Apparently the timeout values below should be read from config
        int connectTimeout = 4;
        int responseTimeout = 16;
        // +JAX-RS 2.1: ClientBuilder.newBuilder().connectTimeout(connectTimeout, TimeUnit.SECONDS)
        // .readTimeout(responseTimeout, TimeUnit.SECONDS).build();
        Client client = new ResteasyClientBuilder()
                .establishConnectionTimeout(connectTimeout, TimeUnit.SECONDS)
                .socketTimeout(responseTimeout, TimeUnit.SECONDS).build();
        return client.target(new URI(url));
    }

    private BigDecimal getExchangeRateForDummyTest(String fromCurrency, String toCurrency) {
        BigDecimal exchangeRate;
        switch (fromCurrency) {
            case "EUR":
                switch (toCurrency) {
                    case "USD":
                        exchangeRate = new BigDecimal("1.14");
                        break;
                    default:
                        exchangeRate = SAME_RATE;
                }
                break;
            case "USD":
                switch (toCurrency) {
                    case "EUR":
                        exchangeRate = new BigDecimal("0.88");
                        break;
                    default:
                        exchangeRate = SAME_RATE;
                }
                break;
            default:
                exchangeRate = SAME_RATE;

        }
        return exchangeRate;
    }
}
