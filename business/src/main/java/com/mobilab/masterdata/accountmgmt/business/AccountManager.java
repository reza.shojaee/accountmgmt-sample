package com.mobilab.masterdata.accountmgmt.business;

import com.mobilab.masterdata.accountmgmt.data.dao.AccountDao;
import com.mobilab.masterdata.accountmgmt.data.dao.Dao;
import com.mobilab.masterdata.accountmgmt.data.entity.Account;
import com.mobilab.masterdata.accountmgmt.data.exception.ValidationException;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.Calendar;

import static com.mobilab.masterdata.accountmgmt.business.BaseBusinessManager.CurrentOperation.PATCH;
import static com.mobilab.masterdata.accountmgmt.business.BaseBusinessManager.CurrentOperation.UPDATE;

/**
 * @author Reza Shojaee, 11/16/18 4:29 AM
 */
@Stateless
public class AccountManager extends BaseBusinessManager<Account> {
    @Inject
    private AccountDao dao;

    // NB! This method should be called in the context of another enclosing transaction to rollback
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public void withdraw(Account account, BigDecimal amount) {
        dao.withdraw(account, amount);
    }

    // NB Same constraint as 'withdraw' above regarding the database transaction semantics
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public void deposit(Account account, BigDecimal amount) {
        dao.deposit(account, amount);
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Account store(Account account) {
        if (account.getTimestamp() == null)
            account.setTimestamp(Calendar.getInstance());
        return super.store(account);
    }

    public Account retrieveByAccountNo(String accountNo) {
        return dao.retrieveByAccountNo(accountNo);
    }

    @Override
    public Dao<Account> getDao() {
        return dao;
    }

    @Override
    public void validate(Account account) {
        if (PATCH == currentOperation) {
            if (account.getAccountNo() != null || account.getCurrencyCode() != null)
                throw new ValidationException("You cannot change your account number or currency");
        } else if (UPDATE == currentOperation) {
            Account currentAccount = dao.retrieve(account.getId());
            if (!account.getAccountNo().equals(currentAccount.getAccountNo())
                    || !account.getCurrencyCode().equals(currentAccount.getCurrencyCode()))
                throw new ValidationException("You cannot change your account number or currency");
        }
    }
}
