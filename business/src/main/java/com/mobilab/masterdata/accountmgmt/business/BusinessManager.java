package com.mobilab.masterdata.accountmgmt.business;

import com.mobilab.masterdata.accountmgmt.data.dao.Dao;

import javax.ejb.Remote;
import java.util.List;

/**
 * @author Reza Shojaee, 7/6/18 5:25 PM
 */
@Remote
public interface BusinessManager<T> {
    Dao<T> getDao();

    T store(T entity);

    List<T> retrieveAll();

    T retrieve(long id);

    T update(T entity);

    T patch(T entity);

    void delete(T entity);

    void delete(long id);
}
