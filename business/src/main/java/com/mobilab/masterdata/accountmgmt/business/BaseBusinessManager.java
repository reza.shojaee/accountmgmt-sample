package com.mobilab.masterdata.accountmgmt.business;

import com.mobilab.masterdata.accountmgmt.data.entity.BaseEntity;
import com.mobilab.masterdata.accountmgmt.data.exception.ValidationException;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import java.util.List;

import static com.mobilab.masterdata.accountmgmt.business.BaseBusinessManager.CurrentOperation.*;
import static com.mobilab.masterdata.accountmgmt.data.entity.BaseEntity.ID_ATTRIBUTE;
import static com.mobilab.masterdata.accountmgmt.data.entity.BaseEntity.LOCK_VERSION_ATTRIBUTE;

/**
 * @author Reza Shojaee, 11/16/18 4:21 AM
 */
public abstract class BaseBusinessManager<T extends BaseEntity>
        implements BusinessManager<T> {
    @SuppressWarnings("WeakerAccess")
    protected CurrentOperation currentOperation;

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public T store(T entity) {
        currentOperation = CREATE;
        validate(entity, false);
        getDao().store(entity);
        return entity;
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<T> retrieveAll() {
        currentOperation = READ;
        return getDao().retrieveAll();
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public T retrieve(long id) {
        currentOperation = READ;
        return getDao().retrieve(id);
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public T update(T entity) {
        currentOperation = UPDATE;
        validate(entity, true);
        return getDao().update(entity);
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public T patch(T entity) {
        currentOperation = PATCH;
        validateMinimum(entity);
        return getDao().patch(entity);
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void delete(T entity) {
        currentOperation = DELETE;
        validateMinimum(entity);
        getDao().delete(entity);
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void delete(long id) {
        currentOperation = DELETE;
        getDao().delete(id);
    }

    private void validate(T entity, boolean forUpdate) {
        if (forUpdate) {
            validateMinimum(entity);
        } else {
            if (entity.getId() != null)
                throw new ValidationException("Id must be null for an entity to be persisted")
                        .errorField(ID_ATTRIBUTE);
        }
        validate(entity);
    }

    private void validateMinimum(T entity) {
        if (entity.getId() == null)
            throw new ValidationException("Id cannot be null").errorField(ID_ATTRIBUTE);
        if (entity.getLockVersion() == null)
            throw new ValidationException("Version cannot be null")
                    .errorField(LOCK_VERSION_ATTRIBUTE);
    }

    // Give a chance to implementors to provide their own custom validation for their entity
    protected void validate(T entity) {}

    public enum CurrentOperation { CREATE, READ, UPDATE, PATCH, DELETE }
}
