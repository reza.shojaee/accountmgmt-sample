package com.mobilab.masterdata.accountmgmt.business;

import com.mobilab.masterdata.accountmgmt.data.dao.Dao;
import com.mobilab.masterdata.accountmgmt.data.entity.BaseEntity;
import com.mobilab.masterdata.accountmgmt.data.exception.ValidationException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

/**
 * @author Reza Shojaee, 11/16/18 4:35 AM
 */
public abstract class BaseBusinessManagerTest<T extends BaseEntity> {
    private static final long TEST_ID = 1000;
    private static final int TEST_LOCK_VERSION = 1;

    BaseBusinessManager<T> businessManager;
    @SuppressWarnings({ "unused", "FieldCanBeLocal" })
    private Class<T> entityClass;

    @SuppressWarnings("WeakerAccess")
    public BaseBusinessManagerTest(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    @Before
    public void setUp() {
        businessManager = makeBusinessManagerInstance();
        when(businessManager.getDao()).thenReturn(mock(getDaoClass()));
    }

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void store() {
        T entity = makeEntityInstance();
        doNothing().when(businessManager.getDao()).store(entity);
        businessManager.store(entity);
        verify(businessManager.getDao()).store(entity);
    }

    @Test
    public void retrieveAll() {
        List<T> expected = new ArrayList<>();
        when(businessManager.getDao().retrieveAll()).thenReturn(expected);
        List<T> actual = businessManager.retrieveAll();
        assertThat(actual, is(equalTo(expected)));
    }

    @Test
    public void retrieveById() {
        T expected = makeEntityInstance();
        when(businessManager.getDao().retrieve(anyLong())).thenReturn(expected);
        T actual = businessManager.retrieve(TEST_ID);
        assertThat(actual, is(equalTo(expected)));
    }

    @Test
    public void update() {
        T entityToUpdate = makeEntityInstance();
        entityToUpdate.setId(TEST_ID);
        entityToUpdate.setLockVersion(TEST_LOCK_VERSION);
        when(businessManager.getDao().update(any())).thenReturn(entityToUpdate);
        prepareForValidation(entityToUpdate);
        T updatedEntity = businessManager.update(entityToUpdate);
        assertThat(updatedEntity, is(equalTo(entityToUpdate)));
    }

    @Test
    public void updateWhileIdMissing() {
        T entityToUpdate = makeEntityInstance();
        entityToUpdate.setLockVersion(TEST_LOCK_VERSION);
        when(businessManager.getDao().update(any())).thenReturn(entityToUpdate);
        prepareForValidation(entityToUpdate);
        exceptionRule.expect(ValidationException.class);
        T updatedEntity = businessManager.update(entityToUpdate);
        assertThat(updatedEntity, is(equalTo(entityToUpdate)));
    }

    @Test
    public void updateWhileLockVersionMissing() {
        T entityToUpdate = makeEntityInstance();
        entityToUpdate.setId(TEST_ID);
        when(businessManager.getDao().update(any())).thenReturn(entityToUpdate);
        prepareForValidation(entityToUpdate);
        exceptionRule.expect(ValidationException.class);
        T updatedEntity = businessManager.update(entityToUpdate);
        assertThat(updatedEntity, is(equalTo(entityToUpdate)));
    }

    @Test
    public void patch() {
        T entityToPatch = makeEntityInstance();
        entityToPatch.setId(TEST_ID);
        entityToPatch.setLockVersion(TEST_LOCK_VERSION);
        when(businessManager.getDao().patch(any())).thenReturn(entityToPatch);
        T patchedEntity = businessManager.patch(entityToPatch);
        assertThat(patchedEntity, is(equalTo(entityToPatch)));
    }

    @Test
    public void patchWhileIdMissing() {
        T entityToPatch = makeEntityInstance();
        entityToPatch.setLockVersion(TEST_LOCK_VERSION);
        when(businessManager.getDao().patch(any())).thenReturn(entityToPatch);
        exceptionRule.expect(ValidationException.class);
        T patchedEntity = businessManager.patch(entityToPatch);
        assertThat(patchedEntity, is(equalTo(entityToPatch)));
    }

    @Test
    public void patchWhileLockVersionMissing() {
        T entityToPatch = makeEntityInstance();
        entityToPatch.setId(TEST_ID);
        when(businessManager.getDao().patch(any())).thenReturn(entityToPatch);
        exceptionRule.expect(ValidationException.class);
        T patchedEntity = businessManager.patch(entityToPatch);
        assertThat(patchedEntity, is(equalTo(entityToPatch)));
    }

    @Test
    public void delete() {
        T entity = makeEntityInstance();
        entity.setId(TEST_ID);
        entity.setLockVersion(TEST_LOCK_VERSION);
        doNothing().when(businessManager.getDao()).delete(any());
        businessManager.delete(entity);
        verify(businessManager.getDao()).delete(entity);
    }

    @Test
    public void deleteWhileIdMissing() {
        T entity = makeEntityInstance();
        entity.setLockVersion(TEST_LOCK_VERSION);
        doNothing().when(businessManager.getDao()).delete(any());
        exceptionRule.expect(ValidationException.class);
        businessManager.delete(entity);
        verify(businessManager.getDao()).delete(entity);
    }

    @Test
    public void deleteWhileLockVersionMissing() {
        T entity = makeEntityInstance();
        entity.setId(TEST_ID);
        doNothing().when(businessManager.getDao()).delete(any());
        exceptionRule.expect(ValidationException.class);
        businessManager.delete(entity);
        verify(businessManager.getDao()).delete(entity);
    }

    @Test
    public void deleteById() {
        doNothing().when(businessManager.getDao()).delete(anyLong());
        businessManager.delete(TEST_ID);
        verify(businessManager.getDao()).delete(TEST_ID);
    }

    void prepareForValidation(T entity) {}

    abstract BaseBusinessManager<T> makeBusinessManagerInstance();

    abstract T makeEntityInstance();

    abstract Class<? extends Dao<T>> getDaoClass();
}
