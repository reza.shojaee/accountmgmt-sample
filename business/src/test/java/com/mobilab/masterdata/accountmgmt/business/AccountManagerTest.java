package com.mobilab.masterdata.accountmgmt.business;

import com.mobilab.masterdata.accountmgmt.data.dao.AccountDao;
import com.mobilab.masterdata.accountmgmt.data.dao.Dao;
import com.mobilab.masterdata.accountmgmt.data.entity.Account;
import org.junit.Before;
import org.junit.Test;

import static com.mobilab.masterdata.accountmgmt.util.reflect.ReflectionUtil.setFieldValue;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Reza Shojaee, 11/16/18 4:44 AM
 */
public class AccountManagerTest extends BaseBusinessManagerTest<Account> {
    public AccountManagerTest() {
        super(Account.class);
    }

    @Before
    public void setUp() {
        businessManager = new AccountManager();
        setFieldValue(businessManager, "dao", mock(AccountDao.class));
    }

    @Test
    public void retrieveById() {
        Account expected = makeEntityInstance();
        when(((AccountDao) businessManager.getDao()).retrieveByAccountNo(anyString()))
                .thenReturn(expected);
        Account actual = ((AccountManager) businessManager).retrieveByAccountNo("1");
        assertThat(actual, is(equalTo(expected)));
    }

    void prepareForValidation(Account entity) {
        entity.setAccountNo("1");
        entity.setCurrencyCode("EUR");
        when(businessManager.getDao().retrieve(anyLong())).thenReturn(entity);
    }

    AccountManager makeBusinessManagerInstance() {
        return new AccountManager();
    }

    Account makeEntityInstance() {
        return new Account();
    }

    @Override
    Class<? extends Dao<Account>> getDaoClass() {
        return AccountDao.class;
    }
}
