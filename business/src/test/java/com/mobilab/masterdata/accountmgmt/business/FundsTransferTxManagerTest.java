package com.mobilab.masterdata.accountmgmt.business;

import com.mobilab.masterdata.accountmgmt.data.dao.Dao;
import com.mobilab.masterdata.accountmgmt.data.dao.FundsTransferTxDao;
import com.mobilab.masterdata.accountmgmt.data.entity.Account;
import com.mobilab.masterdata.accountmgmt.data.entity.FundsTransferTx;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static com.mobilab.masterdata.accountmgmt.util.reflect.ReflectionUtil.setFieldValue;
import static org.mockito.Mockito.*;

/**
 * @author Reza Shojaee, 11/16/18 4:49 AM
 */
public class FundsTransferTxManagerTest extends BaseBusinessManagerTest<FundsTransferTx> {
    public FundsTransferTxManagerTest() {
        super(FundsTransferTx.class);
    }

    @Before
    public void setUp() {
        businessManager = new FundsTransferTxManager();
        setFieldValue(businessManager, "dao", mock(FundsTransferTxDao.class));
    }

    @Test
    public void doTransaction() {
        Account sourceAccount = new Account();
        Account destinationAccount = new Account();
        FundsTransferTx fundsTransferTx = mock(FundsTransferTx.class);
        when(fundsTransferTx.getSourceAccount()).thenReturn(sourceAccount);
        when(fundsTransferTx.getDestinationAccount()).thenReturn(destinationAccount);
        when(fundsTransferTx.getAmount()).thenReturn(new BigDecimal("10.00"));
        ExchangeRateHandler exchangeRateHandler = mock(ExchangeRateHandler.class);
        when(exchangeRateHandler.getExchangeRate(any(), any())).thenReturn(new BigDecimal("1.00"));
        setFieldValue(businessManager, "exchangeRateHandler", exchangeRateHandler);
        AccountManager accountManager = mock(AccountManager.class);
        doNothing().when(accountManager).withdraw(any(Account.class), any(BigDecimal.class));
        doNothing().when(accountManager).deposit(any(Account.class), any(BigDecimal.class));
        setFieldValue(businessManager, "accountManager", accountManager);
        ((FundsTransferTxManager) businessManager).doTransaction(fundsTransferTx);
        verify(accountManager, times(1)).withdraw(sourceAccount, fundsTransferTx.getAmount());
        verify(accountManager, times(1)).deposit(destinationAccount, fundsTransferTx.getAmount());
    }

    FundsTransferTxManager makeBusinessManagerInstance() {
        return new FundsTransferTxManager();
    }

    FundsTransferTx makeEntityInstance() {
        return new FundsTransferTx();
    }

    @Override
    Class<? extends Dao<FundsTransferTx>> getDaoClass() {
        return FundsTransferTxDao.class;
    }
}
