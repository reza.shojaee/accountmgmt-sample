package com.mobilab.masterdata.accountmgmt.util.datetime;

import javax.annotation.Nonnull;
import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

/**
 * @author Reza Shojaee, 11/15/18 5:28 PM
 */
@SuppressWarnings({ "unused", "WeakerAccess" })
public class DateTimeUtil {
    private static final String ISO_8601_DATE_TIME_PATTERN = "yyyy-MM-dd'T'HH:mm:ss";
    private static final String ISO_8601_DATE_TIME_MILLIS_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS";
    private static final String ISO_8601_DATE_TIME_ZONE_PATTERN = "yyyy-MM-dd'T'HH:mm:ssZ";
    private static final String ISO_8601_DATE_TIME_MILLIS_ZONE_PATTERN =
            "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    private static final String ISO_8601_DATE_PATTERN = "yyyy-MM-dd";
    private static final String ISO_8601_TIME_PATTERN = "HH:mm:ss";
    private static final String BASIC_DATE_TIME_MILLIS_PATTERN = "yyyyMMddHHmmssSSS";
    private static final String BASIC_DATE_TIME_PATTERN = "yyyyMMddHHmmss";
    private static final String BASIC_DATE_PATTERN = "yyyyMMdd";
    private static final String BASIC_TIME_PATTERN = "HHmmss";
    private static final String DATE_DOT_PATTERN = "yyyy.MM.dd";
    private static final String DATE_SLASH_PATTERN = "yyyy/MM/dd";

    private static final DateFormat GENERIC_DATE_TIME_PARSER = new GenericDateFormat();
    public static final DateFormat ISO_8601_DATE_TIME_FORMATTER = new GenericDateFormat();
    private static final DateFormat ISO_8601_DATE_TIME_MILLIS_FORMATTER =
            new GenericDateFormat(true, false);
    private static final DateFormat ISO_8601_DATE_TIME_ZONE_FORMATTER =
            new GenericDateFormat(false, true);
    private static final DateFormat ISO_8601_DATE_TIME_MILLIS_ZONE_FORMATTER =
            new GenericDateFormat(true, true);
    private static final DateFormat ISO_8601_DATE_FORMATTER =
            new GenericDateFormat(false, false, true, false);
    private static final DateFormat ISO_8601_TIME_FORMATTER =
            new GenericDateFormat(false, false, false, true);
    private static final ThreadLocal<SimpleDateFormat> BASIC_DATE_TIME_MILLIS_FORMAT =
            ThreadLocal.withInitial(() -> new SimpleDateFormat(BASIC_DATE_TIME_MILLIS_PATTERN));
    private static final ThreadLocal<SimpleDateFormat> BASIC_DATE_TIME_FORMAT =
            ThreadLocal.withInitial(() -> new SimpleDateFormat(BASIC_DATE_TIME_PATTERN));
    private static final ThreadLocal<SimpleDateFormat> BASIC_DATE_FORMAT =
            ThreadLocal.withInitial(() -> new SimpleDateFormat(BASIC_DATE_PATTERN));
    private static final ThreadLocal<SimpleDateFormat> BASIC_TIME_FORMAT =
            ThreadLocal.withInitial(() -> new SimpleDateFormat(BASIC_TIME_PATTERN));
    private static final ThreadLocal<SimpleDateFormat> DATE_DOT_FORMAT =
            ThreadLocal.withInitial(() -> new SimpleDateFormat(DATE_DOT_PATTERN));
    private static final ThreadLocal<SimpleDateFormat> DATE_SLASH_FORMAT =
            ThreadLocal.withInitial(() -> new SimpleDateFormat(DATE_SLASH_PATTERN));

    public static String formatToIso8601(Date dateTime, boolean withMillis, boolean withTimeZone) {
        String formattedDateTime;
        if (withMillis && withTimeZone)
            formattedDateTime = ISO_8601_DATE_TIME_MILLIS_ZONE_FORMATTER.format(dateTime);
        else if (withMillis)
            formattedDateTime = ISO_8601_DATE_TIME_MILLIS_FORMATTER.format(dateTime);
        else if (withTimeZone)
            formattedDateTime = ISO_8601_DATE_TIME_ZONE_FORMATTER.format(dateTime);
        else
            formattedDateTime = ISO_8601_DATE_TIME_FORMATTER.format(dateTime);
        return formattedDateTime;
    }

    public static String formatToIso8601(
            Calendar calendar, boolean withMillis, boolean withTimeZone) {
        return formatToIso8601(calendar.getTime(), withMillis, withTimeZone);
    }

    public static String formatToIso8601(Date dateTime) {
        return formatToIso8601(dateTime, false, false);
    }

    public static String formatToIso8601(Calendar calendar) {
        return formatToIso8601(calendar.getTime(), false, false);
    }

    public static Calendar parseFromIso8601(String dateTime) {
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(GENERIC_DATE_TIME_PARSER.parse(dateTime));
            return calendar;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static class GenericDateFormat extends SimpleDateFormat {
        private static final ThreadLocal<SimpleDateFormat> ISO_8601_DATE_TIME_FORMAT =
                ThreadLocal.withInitial(() -> new SimpleDateFormat(ISO_8601_DATE_TIME_PATTERN));
        private static final ThreadLocal<SimpleDateFormat> ISO_8601_DATE_TIME_MILLIS_FORMAT =
                ThreadLocal.withInitial(
                        () -> new SimpleDateFormat(ISO_8601_DATE_TIME_MILLIS_PATTERN));
        private static final ThreadLocal<SimpleDateFormat> ISO_8601_DATE_TIME_ZONE_FORMAT =
                ThreadLocal.withInitial(
                        () -> new SimpleDateFormat(ISO_8601_DATE_TIME_ZONE_PATTERN));
        private static final ThreadLocal<SimpleDateFormat> ISO_8601_DATE_TIME_MILLIS_ZONE_FORMAT =
                ThreadLocal.withInitial(
                        () -> new SimpleDateFormat(ISO_8601_DATE_TIME_MILLIS_ZONE_PATTERN));
        private static final ThreadLocal<SimpleDateFormat> ISO_8601_DATE_FORMAT =
                ThreadLocal.withInitial(() -> new SimpleDateFormat(ISO_8601_DATE_PATTERN));
        private static final ThreadLocal<SimpleDateFormat> ISO_8601_TIME_FORMAT =
                ThreadLocal.withInitial(() -> new SimpleDateFormat(ISO_8601_TIME_PATTERN));

        private boolean withMillis;
        private boolean withTimeZone;
        private boolean justDate;
        private boolean justTime;

        public GenericDateFormat(
                boolean withMillis, boolean withTimeZone, boolean justDate, boolean justTime) {
            this.withMillis = withMillis;
            this.withTimeZone = withTimeZone;
            this.justDate = justDate;
            this.justTime = justTime;
        }

        public GenericDateFormat(boolean withMillis, boolean withTimeZone) {
            this(withMillis, withTimeZone, false, false);
        }

        public GenericDateFormat() {}

        @Override
        public Date parse(@Nonnull String source, @Nonnull ParsePosition pos) {
            /*if (source.length() - pos.getIndex() == ISO_8601_DATE_TIME_PATTERN.length())
                return ISO_8601_DATE_TIME_FORMAT.get().parse(source, pos);
            return sdf2.parse(source, pos);*/
            Objects.requireNonNull(source, "source");
            Date parsedDate;
            if (source.contains(".") && source.contains("+")) {
                source = source.replaceAll("\\+([0-9]){2}:00", "+0$100");
                parsedDate = ISO_8601_DATE_TIME_MILLIS_ZONE_FORMAT.get().parse(source, pos);
            } else if (source.contains(".")) {
                parsedDate = ISO_8601_DATE_TIME_MILLIS_FORMAT.get().parse(source, pos);
            } else if (source.contains("+")) {
                source = source.replaceAll("\\+([0-9]){2}:00", "+0$100");
                parsedDate = ISO_8601_DATE_TIME_ZONE_FORMAT.get().parse(source, pos);
            } else if (source.length() == 10){
                parsedDate = ISO_8601_DATE_FORMAT.get().parse(source, pos);
            } else if (source.length() == 8){
                parsedDate = ISO_8601_TIME_FORMAT.get().parse(source, pos);
            } else {
                parsedDate = ISO_8601_DATE_TIME_FORMAT.get().parse(source, pos);
            }
            return parsedDate;
        }

        @Override
        public StringBuffer format(
                @Nonnull Date date, @Nonnull StringBuffer toAppendTo,
                @Nonnull FieldPosition fieldPosition) {
            Objects.requireNonNull(date, "date");
            if (withMillis && withTimeZone)
                ISO_8601_DATE_TIME_MILLIS_ZONE_FORMAT.get().format(date, toAppendTo, fieldPosition);
            else if (withMillis)
                ISO_8601_DATE_TIME_MILLIS_FORMAT.get().format(date, toAppendTo, fieldPosition);
            else if (withTimeZone)
                ISO_8601_DATE_TIME_ZONE_FORMAT.get().format(date, toAppendTo, fieldPosition);
            else if (justDate)
                ISO_8601_DATE_FORMAT.get().format(date, toAppendTo, fieldPosition);
            else if (justTime)
                ISO_8601_TIME_FORMAT.get().format(date, toAppendTo, fieldPosition);
            else
                ISO_8601_DATE_TIME_FORMAT.get().format(date, toAppendTo, fieldPosition);
            return toAppendTo;
        }

        public StringBuffer format(
                Calendar calendar, StringBuffer toAppendTo, FieldPosition fieldPosition) {
            Objects.requireNonNull(calendar, "calendar");
            return format(calendar.getTime(), toAppendTo, fieldPosition);
        }
    }
}
