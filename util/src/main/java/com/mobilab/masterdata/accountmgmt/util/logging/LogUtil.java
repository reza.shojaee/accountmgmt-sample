package com.mobilab.masterdata.accountmgmt.util.logging;

import java.util.logging.Level;
import java.util.logging.Logger;

import static com.mobilab.masterdata.accountmgmt.util.reflect.ReflectionUtil.getCurrentExecutingSite;

/**
 * @author Reza Shojaee, 11/16/18 3:44 PM
 */
@SuppressWarnings({ "WeakerAccess", "unused" })
public class LogUtil {
    private LogUtil() {}

    public static void logError(
            Logger logger, Object instance, String message, Throwable throwable) {
        log(logger, Level.SEVERE, instance, message, throwable);
    }

    public static void logError(Logger logger, Object instance, Throwable throwable) {
        log(logger, Level.SEVERE, instance, "", throwable);
    }

    public static void logError(Logger logger, Object instance, String message) {
        log(logger, Level.SEVERE, instance, message);
    }

    public static void logWarning(Logger logger, Object instance, String message) {
        log(logger, Level.WARNING, instance, message);
    }

    public static void logInfo(Logger logger, Object instance, String message) {
        log(logger, Level.INFO, instance, message);
    }

    public static void logConfig(Logger logger, Object instance, String message) {
        log(logger, Level.CONFIG, instance, message);
    }

    public static void logDebug(Logger logger, Object instance, String message) {
        log(logger, Level.FINE, instance, message);
    }

    public static void logTrace(Logger logger, Object instance, String message) {
        log(logger, Level.FINER, instance, message);
    }

    public static void log(Logger logger, Level level, Object instance, String message) {
        log(logger, level, instance, message, null);
    }

    public static void log(
            Logger logger, Level level, Object instance, String message, Throwable throwable) {
        String currentClass = instance.getClass().getName();
        if (instance instanceof Class)
            currentClass = ((Class) instance).getName();
        String executingSite = getCurrentExecutingSite();
        logger.logp(level, currentClass, executingSite, message, throwable);
    }
}
