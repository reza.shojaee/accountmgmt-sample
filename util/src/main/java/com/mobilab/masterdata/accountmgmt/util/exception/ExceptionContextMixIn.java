package com.mobilab.masterdata.accountmgmt.util.exception;

import java.util.Arrays;
import java.util.List;

/**
 * @author Reza Shojaee, 11/15/18 11:40 PM
 */
@SuppressWarnings("unchecked")
public interface ExceptionContextMixIn<T extends RuntimeException> {
    void setErrorCode(String errorCode);

    void setUserMessage(String userMessage);

    List<String> getErrorFields();

    List<String> getErrorConditions();

    default T errorCode(String errorCode) {
        setErrorCode(errorCode);
        return (T) this;
    }

    default T userMessage(String userMessage) {
        setUserMessage(userMessage);
        return (T) this;
    }

    default T errorField(String errorField) {
        getErrorFields().add(errorField);
        return (T) this;
    }

    default T errorField(String... errorFields) {
        getErrorFields().addAll(Arrays.asList(errorFields));
        return (T) this;
    }

    default T errorCondition(String errorCondition) {
        getErrorConditions().add(errorCondition);
        return (T) this;
    }

    default T errorCondition(String... errorConditions) {
        getErrorConditions().addAll(Arrays.asList(errorConditions));
        return (T) this;
    }
}
