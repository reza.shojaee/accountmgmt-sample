package com.mobilab.masterdata.accountmgmt.util.exception;

import java.util.List;
import java.util.function.Function;

import static com.mobilab.masterdata.accountmgmt.util.json.JsonUtil.serializeWithoutQuotes;

/**
 * @author Reza Shojaee, 11/15/18 11:48 PM
 */
public class ExceptionUtil {
    private static final String CONCISE_ERROR_CHAIN_SEPARATOR = " <=> ";

    private ExceptionUtil() {}

    public static String makeConciseExceptionInfo(
            Throwable t, Function<Throwable, String> messageCustomizer) {
        StringBuilder exceptionInfoBuilder = new StringBuilder();
        Throwable exception = t;
        do {
            String errorMessage = exception.getMessage();
            exceptionInfoBuilder.append(exception.getClass().getSimpleName())
                    .append(errorMessage != null ? ": " + errorMessage : "");
            if (messageCustomizer != null) {
                String customizedMessage = messageCustomizer.apply(exception);
                if (customizedMessage != null)
                    exceptionInfoBuilder.append(" - ").append(customizedMessage);
            }
            exception = exception.getCause();
            if (exception != null)
                exceptionInfoBuilder.append(CONCISE_ERROR_CHAIN_SEPARATOR);
        } while (exception != null);
        if (exceptionInfoBuilder.charAt(exceptionInfoBuilder.length() - 1) == '\n')
            exceptionInfoBuilder.deleteCharAt(exceptionInfoBuilder.length() - 1);
        return exceptionInfoBuilder.toString();
    }

    public static String makeConciseExceptionInfo(Throwable throwable) {
        return makeConciseExceptionInfo(throwable, exception -> {
            if (exception instanceof ExceptionContextMixIn) {
                ExceptionContextMixIn contextInfo = (ExceptionContextMixIn<?>) exception;
                if (contextInfo.getErrorFields() != null
                        || contextInfo.getErrorConditions() != null) {
                    return serializeWithoutQuotes(new Object() {
                        @SuppressWarnings("unused")
                        public List<String> getErrorFields() {
                            List<String> errorFields =
                                    ((ExceptionContextMixIn<?>) exception).getErrorFields();
                            return errorFields == null || errorFields.isEmpty()
                                    ? null : errorFields;
                        }

                        @SuppressWarnings("unused")
                        public List<String> getErrorConditions() {
                            List<String> errorConditions =
                                    ((ExceptionContextMixIn<?>) exception).getErrorConditions();
                            return errorConditions == null || errorConditions.isEmpty()
                                    ? null : errorConditions;
                        }
                    });
                }
            }
            return null;
        });
    }
}
