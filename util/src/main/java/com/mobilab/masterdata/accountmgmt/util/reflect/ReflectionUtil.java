package com.mobilab.masterdata.accountmgmt.util.reflect;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import javax.annotation.Nonnull;
import java.beans.FeatureDescriptor;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.beans.Transient;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.function.Predicate;

import static java.util.Arrays.asList;
import static java.util.Objects.requireNonNull;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

/**
 * @author Reza Shojaee, 11/15/18 11:11 PM
 */
@SuppressWarnings({ "unused", "WeakerAccess" })
public class ReflectionUtil {
    private ReflectionUtil() {}

    private static LoadingCache<Class<?>, List<PropertyDescriptor>> propertiesCache =
            CacheBuilder.newBuilder().weakKeys().build(
                    new CacheLoader<Class<?>, List<PropertyDescriptor>>() {
                        public List<PropertyDescriptor> load(@Nonnull() Class<?> clazz) {
                            PropertyDescriptor[] classDescriptors = findPropertyDescriptors(clazz);
                            List<PropertyDescriptor> propertyDescriptors =
                                    asList(classDescriptors);
                            propertyDescriptorsMap.put(clazz, propertyDescriptors.stream()
                                    .collect(toMap(FeatureDescriptor::getName, identity())));
                            return propertyDescriptors;
                        }
                    });
    private static Map<Class<?>, Map<String, PropertyDescriptor>> propertyDescriptorsMap =
            new HashMap<>();
    private static Map<Class<?>, Map<String, Field>> fieldsMap = new HashMap<>();

    @SuppressWarnings("WeakerAccess")
    public static Map<String, Object> findPropertiesValues(
            Object bean, Predicate<PropertyDescriptor> withInclusionPredicate) {
        return propertiesCache.getUnchecked(bean.getClass()).stream()
                .filter(withInclusionPredicate)
                .collect(HashMap::new, (map, propertyDescriptor) -> {
                    try {
                        map.put(propertyDescriptor.getName(),
                                propertyDescriptor.getReadMethod().invoke(bean));
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                }, HashMap::putAll);
    }

    @SuppressWarnings("WeakerAccess")
    public static Map<String, Object> findPropertiesValues(Object bean) {
        return findPropertiesValues(bean, propertyDescriptor -> true);
    }

    public static Map<String, Object> findPersistentPropertiesValues(Object bean) {
        return findPropertiesValues(bean, ReflectionUtil::includePersistableProperty);
    }

    public static void setCorrespondingProperties(Object sourceBean, Object targetBean) {
        requireNonNull(sourceBean);
        requireNonNull(targetBean);
        Map<String, PropertyDescriptor> targetBeanProperties =
                propertyDescriptorsMap.get(targetBean.getClass());
        if (targetBeanProperties == null)
            // Load cache with this class property descriptors
            propertiesCache.getUnchecked(targetBean.getClass());
        Map<String, PropertyDescriptor> targetProperties =
                propertyDescriptorsMap.get(targetBean.getClass());
        findPropertiesValues(sourceBean).forEach((propertyName, value) -> {
            PropertyDescriptor propertyDescriptor = targetProperties.get(propertyName);
            if (propertyDescriptor != null && propertyDescriptor.getWriteMethod() != null) {
                try {
                    propertyDescriptor.getWriteMethod().invoke(targetBean, value);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        });
    }

    public static Object getPropertyValue(Object bean, String propertyName) {
        Map<String, PropertyDescriptor> beanProperties =
                propertyDescriptorsMap.get(bean.getClass());
        if (beanProperties != null) {
            try {
                return beanProperties.get(propertyName).getReadMethod().invoke(bean);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } else {
            return getFieldValue(bean, propertyName);
        }
    }

    public static void setPropertyValue(Object bean, String propertyName, Object value) {
        Map<String, PropertyDescriptor> beanProperties =
                propertyDescriptorsMap.get(bean.getClass());
        if (beanProperties != null) {
            try {
                beanProperties.get(propertyName).getWriteMethod().invoke(bean, value);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } else {
            setFieldValue(bean, propertyName, value);
        }
    }

    public static Object getFieldValue(Object object, String fieldName) {
        Class<?> clazz = object.getClass();
        try {
            Field field = findField(clazz, fieldName);
            if (field == null)
                throw new NoSuchFieldException(String.format(
                        "Field '%s' does not exist in type hierarchy '%s'", fieldName, clazz));
            field.setAccessible(true);
            return field.get(object);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void setFieldValue(Object object, String fieldName, Object value) {
        Class<?> clazz = object.getClass();
        try {
            Field field = findField(clazz, fieldName);
            if (field == null)
                throw new NoSuchFieldException(String.format(
                        "Field '%s' does not exist in type hierarchy '%s'", fieldName, clazz));
            field.setAccessible(true);
            field.set(object, value);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    // Finds field either in the clazz or one of its superclasses, wherever found
    public static Field findField(Class<?> clazz, String fieldName) {
        Map<String, Field> classFields = fieldsMap.get(clazz);
        if (classFields != null) {
            Field field = classFields.get(fieldName);
            if (field != null)
                return field;
        } else {
            classFields = new HashMap<>();
            fieldsMap.put(clazz, classFields);
        }
        do {
            Field[] fields = clazz.getDeclaredFields();
            for (Field aField : fields) {
                if (fieldName.equals(aField.getName())) {
                    classFields.put(fieldName, aField);
                    return aField;
                }
            }
            clazz = clazz.getSuperclass();
        } while (clazz != null);
        return null;
    }

    public static String getCurrentExecutingSite() {
        return getExecutingSite(4);
    }

    private static String getExecutingSite(@SuppressWarnings("SameParameterValue") int depth) {
        return getExecutingMethod(depth) + "@" + getExecutingLine(depth);
    }

    private static String getExecutingMethod(int depth) {
        StackTraceElement[] currentStackTrace = Thread.currentThread().getStackTrace();
        if (currentStackTrace.length < depth)
            return "N/A";
        return currentStackTrace[depth - 1].getMethodName();
    }

    private static int getExecutingLine(int depth) {
        StackTraceElement[] currentStackTrace = Thread.currentThread().getStackTrace();
        if (currentStackTrace.length < depth)
            return 0;
        return currentStackTrace[depth - 1].getLineNumber();
    }

    private static PropertyDescriptor[] findPropertyDescriptors(Class<?> clazz) {
        try {
            return Arrays.stream(
                    Introspector.getBeanInfo(clazz, Object.class).getPropertyDescriptors())
                    // filter out properties with setters only
                    .filter(descriptor -> Objects.nonNull(descriptor.getReadMethod()))
                    .toArray(PropertyDescriptor[]::new);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static Boolean includePersistableProperty(PropertyDescriptor propertyDescriptor) {
        Method readMethod = propertyDescriptor.getReadMethod();
        if (Modifier.isStatic(readMethod.getModifiers()))
            return false;
        if (Collection.class.isAssignableFrom(readMethod.getReturnType()))
            return false;
        return readMethod.getAnnotation(Transient.class) == null;
        //return true;
    }
}
