package com.mobilab.masterdata.accountmgmt.util.exception;

/**
 * @author Reza Shojaee, 11/15/18 11:44 PM
 */
public class NotApplicableException extends GenericException {
    public NotApplicableException() {
        super("The operation does not make sense in the current context "
                + "(the call should not normally reach here in the first place)!");
    }

    public NotApplicableException(String message) {
        super(message);
    }

    public NotApplicableException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotApplicableException(Throwable cause) {
        super(cause);
    }

    public NotApplicableException(
            String message, Throwable cause,
            boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
