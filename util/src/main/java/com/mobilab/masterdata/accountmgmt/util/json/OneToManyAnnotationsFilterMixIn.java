package com.mobilab.masterdata.accountmgmt.util.json;

import com.fasterxml.jackson.annotation.JsonFilter;

/**
 * @author Reza Shojaee, 11/15/18 7:02 PM
 */
@JsonFilter("OneToManyAnnotationsFilter")
class OneToManyAnnotationsFilterMixIn {}
