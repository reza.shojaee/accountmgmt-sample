package com.mobilab.masterdata.accountmgmt.util.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.BeanPropertyWriter;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.PropertyWriter;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;

import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import java.lang.annotation.Annotation;

import static com.mobilab.masterdata.accountmgmt.util.datetime.DateTimeUtil.ISO_8601_DATE_TIME_FORMATTER;

/**
 * @author Reza Shojaee, 11/15/18 4:34 PM
 */
@SuppressWarnings({ "WeakerAcess", "unused" })
public class JsonUtil {
    private static ObjectMapper defaultSerializer = new ObjectMapper();
    private static ObjectMapper noQuoteSerializer = new ObjectMapper();

    static {
        defaultSerializer.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        defaultSerializer.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        defaultSerializer.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, false);
        defaultSerializer.configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true);
        defaultSerializer.setDateFormat(ISO_8601_DATE_TIME_FORMATTER);
        defaultSerializer.addMixIn(Object.class, OneToManyAnnotationsFilterMixIn.class);
        FilterProvider filterProvider = new SimpleFilterProvider()
                .addFilter("OneToManyAnnotationsFilter", new SimpleBeanPropertyFilter() {
                    @Override
                    public void serializeAsField(
                            Object object, JsonGenerator jsonGenerator,
                            SerializerProvider serializerProvider, PropertyWriter propertyWriter)
                            throws Exception {
                        if (include(propertyWriter))
                            propertyWriter.serializeAsField(
                                    object, jsonGenerator, serializerProvider);
                    }

                    @Override
                    protected boolean include(BeanPropertyWriter beanPropertyWriter) {
                        return include((PropertyWriter) beanPropertyWriter);
                    }

                    @Override
                    protected boolean include(PropertyWriter propertyWriter) {
                        Annotation oneToManyAnnotation =
                                propertyWriter.getAnnotation(OneToMany.class);
                        if (oneToManyAnnotation != null)
                            return false;
                        Annotation manyToManyAnnotation =
                                propertyWriter.getAnnotation(ManyToMany.class);
                        return manyToManyAnnotation == null;
                    }
                });
        defaultSerializer.setFilterProvider(filterProvider);

        noQuoteSerializer.configure(JsonGenerator.Feature.QUOTE_FIELD_NAMES, false);
        noQuoteSerializer.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
        noQuoteSerializer.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        noQuoteSerializer.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, false);
        noQuoteSerializer.configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true);
        noQuoteSerializer.setDateFormat(ISO_8601_DATE_TIME_FORMATTER);
        noQuoteSerializer.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }

    private JsonUtil() {}

    public static String serialize(Object object) {
        if (object == null)
            return null;
        try {
            return defaultSerializer.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public static String serializeWithoutQuotes(Object object) {
        if (object == null)
            return null;
        try {
            return noQuoteSerializer.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> T deserialize(String jsonString, Class<T> targetType) {
        if (jsonString == null)
            return null;
        try {
            return defaultSerializer.readValue(jsonString, targetType);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> T deserialize(String jsonString, TypeReference<T> typeInstance) {
        if (jsonString == null)
            return null;
        try {
            return defaultSerializer.readValue(jsonString, typeInstance);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }}
