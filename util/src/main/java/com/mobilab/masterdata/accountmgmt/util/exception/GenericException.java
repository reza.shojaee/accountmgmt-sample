package com.mobilab.masterdata.accountmgmt.util.exception;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Reza Shojaee, 11/15/18 11:45 PM
 */
@SuppressWarnings("WeakerAccess")
public class GenericException extends RuntimeException
        implements ExceptionContextMixIn<GenericException> {
    private String errorCode;
    private String userMessage;
    private List<String> errorFields = new ArrayList<>();
    private List<String> errorConditions = new ArrayList<>();

    public GenericException() {}

    public GenericException(String message) {
        super(message);
    }

    public GenericException(String message, Throwable cause) {
        super(message, cause);
    }

    public GenericException(Throwable cause) {
        super(cause);
    }

    public GenericException(
            String message, Throwable cause,
            boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getUserMessage() {
        return userMessage;
    }

    public void setUserMessage(String userMessage) {
        this.userMessage = userMessage;
    }

    public List<String> getErrorFields() {
        return errorFields;
    }

    public void setErrorFields(List<String> errorFields) {
        this.errorFields = errorFields;
    }

    public List<String> getErrorConditions() {
        return errorConditions;
    }

    public void setErrorConditions(List<String> errorConditions) {
        this.errorConditions = errorConditions;
    }
}
