package com.mobilab.masterdata.accountmgmt.util.reflect;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import static com.mobilab.masterdata.accountmgmt.util.reflect.ReflectionUtil.*;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

/**
 * @author Reza Shojaee, 11/15/18 11:17 PM
 */
public class ReflectionUtilTest {
    @Test
    public void findCommonPropertiesValues() {
        Map<String, Object> propertiesValues = findPropertiesValues(new SampleTestClass());
        assertThat(propertiesValues.keySet(),
                hasItems("id", "remarks", "timestamp", "lockVersion"));
        assertThat(propertiesValues.get("timestamp"), is(notNullValue()));
    }

    @Test
    public void findCommonPersistentPropertiesValues() {
        Map<String, Object> propertiesValues =
                findPersistentPropertiesValues(new SampleTestClass());
        assertThat(propertiesValues.keySet(),
                hasItems("id", "remarks", "timestamp", "lockVersion"));
        assertThat(propertiesValues.keySet(), not(hasItems("updateExcludeFields")));
        assertThat(propertiesValues.get("timestamp"), is(notNullValue()));
    }

    @Test
    public void makeSureGetsCurrentExecutingSite() {
        assertThat(getCurrentExecutingSite(), startsWith("getCurrentExecutingSite"));
    }

    @SuppressWarnings("unused")
    private static class SampleTestClass {
        private static final int CONST = 0;

        private static List<String> updateExcludeFields = new ArrayList<>();

        private Long id;
        private String remarks;
        private Calendar timestamp = Calendar.getInstance();
        private Integer lockVersion;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getRemarks() {
            return remarks;
        }

        public void setRemarks(String remarks) {
            this.remarks = remarks;
        }

        public Calendar getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(Calendar timestamp) {
            this.timestamp = timestamp;
        }

        public Integer getLockVersion() {
            return lockVersion;
        }

        public void setLockVersion(Integer lockVersion) {
            this.lockVersion = lockVersion;
        }
    }
}
