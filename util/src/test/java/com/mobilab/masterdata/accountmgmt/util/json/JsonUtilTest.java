package com.mobilab.masterdata.accountmgmt.util.json;

import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.Test;

import java.util.Calendar;
import java.util.Map;
import java.util.Objects;

import static com.mobilab.masterdata.accountmgmt.util.datetime.DateTimeUtil.formatToIso8601;
import static com.mobilab.masterdata.accountmgmt.util.json.JsonUtil.deserialize;
import static com.mobilab.masterdata.accountmgmt.util.json.JsonUtil.serialize;
import static com.mobilab.masterdata.accountmgmt.util.json.JsonUtilTest.SampleBean.*;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * @author Reza Shojaee, 11/15/18 7:19 PM
 */
public class JsonUtilTest {
    private static final SampleBean SAMPLE_BEAN =
            new SampleBean("4587092490854", "EUR", "Reza Shojaee", Calendar.getInstance());
    private static final String MANUALLY_SERIALIZED_SAMPLE_BEAN = serializeSampleBeanManually();

    @Test
    public void serializeAnyPojo() {
        assertThat(serialize(SAMPLE_BEAN), is(equalTo(MANUALLY_SERIALIZED_SAMPLE_BEAN)));
    }

    @Test
    public void deserializeAnyPojo() {
        SampleBean sampleBean = deserialize(MANUALLY_SERIALIZED_SAMPLE_BEAN, SampleBean.class);
        assertThat(sampleBean.accountNo, is(equalTo(SAMPLE_BEAN.accountNo)));
        assertThat(sampleBean.currencyCode, is(equalTo(SAMPLE_BEAN.currencyCode)));
        assertThat(sampleBean.holderName, is(equalTo(SAMPLE_BEAN.holderName)));
        assertThat(formatToIso8601(sampleBean.timestamp),
                is(equalTo(formatToIso8601(SAMPLE_BEAN.timestamp))));
    }

    @Test
    public void deserializeToGenericMap() {
        Map<String, Object> deserializedMap = deserialize(MANUALLY_SERIALIZED_SAMPLE_BEAN,
                new TypeReference<Map<String, Object>>() {});
        assertThat(deserializedMap.get(ACCOUNT_NO_FIELD), is(equalTo(SAMPLE_BEAN.accountNo)));
        assertThat(deserializedMap.get(CURRENCY_CODE_FIELD), is(equalTo(SAMPLE_BEAN.currencyCode)));
        assertThat(deserializedMap.get(HOLDER_NAME_FIELD), is(equalTo(SAMPLE_BEAN.holderName)));
        assertThat(deserializedMap.get(TIMESTAMP_FIELD),
                is(equalTo(formatToIso8601(SAMPLE_BEAN.timestamp))));
    }

    private static String serializeSampleBeanManually() {
        return String.format("{\"%s\":\"%s\",\"%s\":\"%s\",\"%s\":\"%s\",\"%s\":\"%s\"}",
                ACCOUNT_NO_FIELD, SAMPLE_BEAN.accountNo,
                CURRENCY_CODE_FIELD, SAMPLE_BEAN.currencyCode,
                HOLDER_NAME_FIELD, SAMPLE_BEAN.holderName,
                TIMESTAMP_FIELD, formatToIso8601(SAMPLE_BEAN.timestamp));
    }

    @SuppressWarnings({ "unused", "WeakerAccess" })
    static class SampleBean {
        static final String ACCOUNT_NO_FIELD = "accountNo";
        static final String CURRENCY_CODE_FIELD = "currencyCode";
        static final String HOLDER_NAME_FIELD = "holderName";
        static final String TIMESTAMP_FIELD = "timestamp";

        private String accountNo;
        private String currencyCode;
        private String holderName;
        private Calendar timestamp;

        SampleBean(String accountNo, String currencyCode, String holderName, Calendar timestamp) {
            this.accountNo = accountNo;
            this.currencyCode = currencyCode;
            this.holderName = holderName;
            setTimestamp(timestamp);
        }

        SampleBean() {}

        @Override
        public boolean equals(Object other) {
            if (other == null)
                return false;
            if (this == other)
                return true;
            if (getClass() != other.getClass())
                return false;
            SampleBean that = (SampleBean) other;
            return Objects.equals(accountNo, that.accountNo)
                    && Objects.equals(currencyCode, that.currencyCode)
                    && Objects.equals(holderName, that.holderName)
                    && Objects.equals(
                            formatToIso8601(timestamp), formatToIso8601(that.timestamp));
            //return Objects.deepEquals(this, that);
        }

        @Override
        public int hashCode() {
            return Objects.hash(accountNo, currencyCode, holderName, timestamp);
        }

        public String getAccountNo() {
            return accountNo;
        }

        public void setAccountNo(String accountNo) {
            this.accountNo = accountNo;
        }

        public String getCurrencyCode() {
            return currencyCode;
        }

        public void setCurrencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
        }

        public String getHolderName() {
            return holderName;
        }

        public void setHolderName(String holderName) {
            this.holderName = holderName;
        }

        public Calendar getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(Calendar timestamp) {
            this.timestamp = timestamp;
            // For serialization test purposes, just ignore milliseconds
            this.timestamp.set(Calendar.MILLISECOND, 0);
        }
    }
}
