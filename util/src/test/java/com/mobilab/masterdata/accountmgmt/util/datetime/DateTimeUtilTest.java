package com.mobilab.masterdata.accountmgmt.util.datetime;

import org.junit.Test;

import java.util.Calendar;
import java.util.TimeZone;

import static com.mobilab.masterdata.accountmgmt.util.datetime.DateTimeUtil.formatToIso8601;
import static com.mobilab.masterdata.accountmgmt.util.datetime.DateTimeUtil.parseFromIso8601;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * @author Reza Shojaee, 11/15/18 7:19 PM
 */
public class DateTimeUtilTest {
    @Test
    public void parseWithMillisAndTimeZone() {
        Calendar now = Calendar.getInstance();
        assertThat(now, is(equalTo(parseFromIso8601(formatToIso8601(now, true, true)))));
    }

    @Test
    public void parseWhileMillisOmitted() {
        Calendar nowMillisecondsOmitted = Calendar.getInstance();
        nowMillisecondsOmitted.set(Calendar.MILLISECOND, 0);
        assertThat(nowMillisecondsOmitted, is(equalTo(
                parseFromIso8601(formatToIso8601(nowMillisecondsOmitted, false, true)))));
    }

    @Test
    public void parseWhileTimeZoneOmitted() {
        Calendar nowTimeZoneOmitted = Calendar.getInstance(TimeZone.getTimeZone("00:00"));
        nowTimeZoneOmitted.setTimeZone(TimeZone.getDefault());
        assertThat(nowTimeZoneOmitted, is(equalTo(
                parseFromIso8601(formatToIso8601(nowTimeZoneOmitted, true, false)))));
    }

    @Test
    public void parseWhileMillisAndTimeZoneOmitted() {
        Calendar nowMillisecondsAndTimeZoneOmitted =
                Calendar.getInstance(TimeZone.getTimeZone("00:00"));
        nowMillisecondsAndTimeZoneOmitted.set(Calendar.MILLISECOND, 0);
        nowMillisecondsAndTimeZoneOmitted.setTimeZone(TimeZone.getDefault());
        assertThat(nowMillisecondsAndTimeZoneOmitted, is(equalTo(
                parseFromIso8601(formatToIso8601(nowMillisecondsAndTimeZoneOmitted)))));
    }
}
